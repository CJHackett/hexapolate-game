﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdController : MonoBehaviour {
    
    public int gamesBeforeAd;
    private ShowOptions preGameCallback;
    public MainMenuNavigation menu;
    public int rewardStartLevel = 5;
    int noRewardStartLevel;

    void Start()
    {
        noRewardStartLevel = 0;
        preGameCallback = new ShowOptions { resultCallback = HandleAdvertFinish_PreGame };

        if (Advertisement.isSupported && !Advertisement.isInitialized)
            Advertisement.Initialize("3189463", false);
    }

    public void OpenPreGameAd(int _noRewardStartLevel)
    {
        noRewardStartLevel = _noRewardStartLevel;

        if (Advertisement.IsReady())
        {
            if ((PlayerPrefs.GetInt("GamesSinceAd") >= gamesBeforeAd))
            {
                PlayerPrefs.SetInt("GamesSinceAd", 0);
                Advertisement.Show(preGameCallback);
            }
            else
            {
                PlayerPrefs.SetInt("GamesSinceAd", (PlayerPrefs.GetInt("GamesSinceAd") + 1));
                menu.StartGame(noRewardStartLevel);
            }
        }else
            menu.StartGame(noRewardStartLevel);
    }

    private void HandleAdvertFinish_PreGame(ShowResult result)
    {
        if (result == ShowResult.Finished)
            menu.StartGame(noRewardStartLevel+rewardStartLevel);
        else
            menu.StartGame(noRewardStartLevel);
    }
}
