﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour
{
    //singleton
    public static PlayerScore instance = null;
    [SerializeField] Text scoreText;
    [SerializeField] Text leadingZerosText;
    [SerializeField] int leadingZerosNum = 12;

    int currentScore;
    public int CurrentScore { get => currentScore;}

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        currentScore = 0;
        UpdateScore(0);
    }

    public void UpdateScore(int score)
    {
        currentScore += (score * (DifficultyController.currentLevel+1));
        int log = 0;
        if (score > 0)
            log = Mathf.FloorToInt(Mathf.Log10(currentScore));

        string zeros = "";
        for (int i = 0; i < leadingZerosNum - log; i++)
            zeros += "0";
        zeros += currentScore.ToString();
        scoreText.text = currentScore.ToString();//#,##0
        leadingZerosText.text = zeros;
    }
}
