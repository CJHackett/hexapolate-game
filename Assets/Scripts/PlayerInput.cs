﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SwipeDirection { None = 0, Left = 1, Right = 2, Up = 4, Down = 8 }

public class PlayerInput : MonoBehaviour
{
    private static PlayerInput instance;
    public static PlayerInput Instance { get { return instance; } }
    HexNode cameraTarget;

    public SwipeDirection Direction { set; get; }
    Vector3 touchPosition;
    [HideInInspector] public float integratedDistanceX, integratedDistanceY;
    float distancePerTileMove = 10f;

    float swipeResistanceX = 0.0f;
    float swipeResistanceY = 0.0f;
    float swipePosDampingX;
    float swipePosDampingY;
    public float xSensitivity { set { swipePosDampingX = value; } get { return swipePosDampingX; } }
    public float ySensitivity { set { swipePosDampingY = value; } get { return swipePosDampingY; } }

    float angleY = 60f;
    float angleX = 50f;

    public const float maxSensitivity = 1;
    public const float minSensitivity = 10;

    const float canvasScaleX = 2160;
    const float canvasScaleY = 1080;
    const float canvasFactorX = 0.5f;
    const float canvasFactorY = 0.5f;
    public RectTransform leftButtonRect;
    public RectTransform rightButtonRect;
    public RectTransform downLeftButtonRect;
    public RectTransform downRightButtonRect;

    Vector2 startLeftButton;
    Vector2 endLeftButton;
    Vector2 startRightButton;
    Vector2 endRightButton;
    Vector2 startDownButton;
    Vector2 endDownButton;

    public RectTransform canvas;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }

    private void Start()
    {
        Direction = SwipeDirection.None;
        cameraTarget = HexNodeMap.instance.GetSpawnHex();

        xSensitivity = GameSettings.instance.XSensitivity;
        ySensitivity = GameSettings.instance.YSensitivity;

        UpdateButtonPoints();
    }

    public void UpdateButtonPoints()
    {
        float screenSizeX = canvas.rect.width;
        float screenSizeY = canvas.rect.height;

        startLeftButton = new Vector2(
            ((leftButtonRect.anchorMin.x * screenSizeX) + leftButtonRect.anchoredPosition.x - (leftButtonRect.rect.width / 2)) / screenSizeX,
            ((leftButtonRect.anchorMin.y * screenSizeY) + leftButtonRect.anchoredPosition.y - (leftButtonRect.rect.height / 2)) / screenSizeY);
        endLeftButton = new Vector2(
            startLeftButton.x + (leftButtonRect.rect.width / screenSizeX),
            startLeftButton.y + (leftButtonRect.rect.height / screenSizeY));

        startRightButton = new Vector2(
            ((rightButtonRect.anchorMin.x * screenSizeX) + rightButtonRect.anchoredPosition.x - (rightButtonRect.rect.width / 2)) / screenSizeX,
            ((rightButtonRect.anchorMin.y * screenSizeY) + rightButtonRect.anchoredPosition.y - (rightButtonRect.rect.height / 2)) / screenSizeY);
        endRightButton = new Vector2(
            startRightButton.x + (rightButtonRect.rect.width / screenSizeX),
            startRightButton.y + (rightButtonRect.rect.height / screenSizeY));

        startDownButton = new Vector2(
            ((downLeftButtonRect.anchorMin.x * screenSizeX) + downLeftButtonRect.anchoredPosition.x - (downLeftButtonRect.rect.width / 2)) / screenSizeX,
            ((downLeftButtonRect.anchorMin.y * screenSizeY) + downLeftButtonRect.anchoredPosition.y - (downLeftButtonRect.rect.height / 2)) / screenSizeY);
        endDownButton = new Vector2(
            startDownButton.x + 1.0f,
            startDownButton.y + (downLeftButtonRect.rect.height / screenSizeY));
    }

    //if held down, only x axis is taken into consideration. When held, movements directly translate into a position change,
    //when released and produces a large enough flick, velocity is added to the camera
    public void Update()
    {
        if (Time.deltaTime > 0)
        {
            if (GameSettings.instance.UseSwipeControls)
            {
                //Pressed
                if (Input.GetMouseButtonDown(0))
                {
                    touchPosition = Input.mousePosition;
                }
                //Held
                else if (Input.GetMouseButton(0))
                {
                    //determine direction
                    
                    Vector2 deltaPosition = touchPosition - Input.mousePosition;
                    touchPosition = Input.mousePosition;

                    float deltaAngle = Vector2.Angle(Vector2.up, deltaPosition);
                    if (Mathf.Abs(deltaPosition.x) > swipeResistanceX && (deltaAngle > angleX) && (deltaAngle < 180 - angleX))
                    {
                        //swipe is x
                        integratedDistanceX += deltaPosition.x / swipePosDampingX;
                    }
                    else if (Mathf.Abs(deltaPosition.y) > swipeResistanceY && (deltaAngle <= angleY))
                    {
                        //swipe is y
                        integratedDistanceY += deltaPosition.y / swipePosDampingY;
                    }
                }
            }
            else
            {
                if (Input.GetMouseButton(0))
                {
                    Vector2 mouseScreenPosition = new Vector2(Input.mousePosition.x/Screen.width, Input.mousePosition.y/Screen.height);
                    if (IsPressingLeftButton(mouseScreenPosition))
                    {
                        integratedDistanceX -= ((endLeftButton.x - mouseScreenPosition.x)*100) / swipePosDampingX;
                    }
                    else if (IsPressingRightButton(mouseScreenPosition))
                    {
                        integratedDistanceX += ((mouseScreenPosition.x - startRightButton.x) * 100) / swipePosDampingX;
                    }
                    else if (IsPressingDownButton(mouseScreenPosition))
                    {
                        integratedDistanceY += ((endDownButton.y - mouseScreenPosition.y)* 100) / swipePosDampingY;
                    }
                }
            }
                //Released
                /*
                else if (Input.GetMouseButtonUp(0))
                {
                    //determine direction
                    Vector2 deltaPosition = touchPosition - Input.mousePosition;
                    touchPosition = Input.mousePosition;
                    float deltaAngle = Vector2.Angle(Vector2.up, deltaPosition);
                    Debug.Log(deltaAngle);
                    if (Mathf.Abs(deltaPosition.x) > swipeResistanceX && (deltaAngle > angleX) && (deltaAngle < 180 - angleX))
                    {
                        //swipe is x
                        swipeVelocity = deltaPosition.x /swipeVelDampingX;
                        if (deltaPosition.x < 0)
                        {
                            if (swipeVelocity < -velocityClamp)
                                swipeVelocity = -velocityClamp;
                            //Debug.Log("Swipe detected with velocity: " + swipeVelocity + " and direction: left");
                        }
                        else
                        {
                            if (swipeVelocity > velocityClamp)
                                swipeVelocity = velocityClamp;
                            //Debug.Log("Swipe detected with velocity: " + swipeVelocity + " and direction: right");
                        }

                    }
                    if (Mathf.Abs(deltaPosition.y) > swipeResistanceY && (deltaAngle <= angleY))
                    {
                        //swipe is y
                        if (deltaPosition.y > 0)
                        {
                            if (HexTile.activeHex != null && !swipedDownThisMotion)
                            {
                                HexTile.activeHex.CommandHexDown();
                                swipedDownThisMotion = true;
                                Debug.Log("Swipe detected down");
                            }
                        }

                    }
                }


                if (swipeVelocity != 0 && !ignoreVelocity)
                {
                    integratedDistanceX += swipeVelocity * Time.deltaTime;
                    //Debug.Log("Velocity: " + swipeVelocity + " and integrated distance: " + integratedDistance);
                    if (swipeVelocity < 0)
                    {
                        swipeVelocity += swipeDeceleration * Time.deltaTime;
                        if (swipeVelocity > 0)
                            swipeVelocity = 0;
                    }
                    else
                    {
                        swipeVelocity -= swipeDeceleration * Time.deltaTime;
                        if (swipeVelocity < 0)
                            swipeVelocity = 0;
                    }
                }
                */
            
                if (integratedDistanceX != 0)
                {
                    if (integratedDistanceX <= -distancePerTileMove)
                    {
                        do
                        {
                            integratedDistanceX += distancePerTileMove;
                            ShiftLeft();
                        } while (integratedDistanceX <= -distancePerTileMove);
                    }
                    else if (integratedDistanceX >= distancePerTileMove)
                    {
                        do
                        {
                            integratedDistanceX -= distancePerTileMove;
                            ShiftRight();
                        } while (integratedDistanceX >= distancePerTileMove);
                    }
                }
                if (integratedDistanceY != 0)
                {
                    if (integratedDistanceY >= distancePerTileMove)
                    {
                        do
                        {
                            integratedDistanceY -= distancePerTileMove;
                            ShiftDown();
                        } while (integratedDistanceY >= distancePerTileMove);
                    }
                }
            
            if (HexTile.activeHex != null && HexTile.activeHex.hexIsControllable)
                cameraTarget = HexTile.activeHex.CurrNode;
            CameraController.instance.FollowPoint(cameraTarget.position);
        }
    }

    bool IsPressingLeftButton(Vector2 mouseScreenPosition)
    {
        return (
            mouseScreenPosition.x > startLeftButton.x && mouseScreenPosition.x <= endLeftButton.x &&
            mouseScreenPosition.y > startLeftButton.y && mouseScreenPosition.y <= endLeftButton.y);
    }
    bool IsPressingRightButton(Vector2 mouseScreenPosition)
    {
        return (
            mouseScreenPosition.x > startRightButton.x && mouseScreenPosition.x <= endRightButton.x &&
            mouseScreenPosition.y > startRightButton.y && mouseScreenPosition.y <= endRightButton.y);
    }
    bool IsPressingDownButton(Vector2 mouseScreenPosition)
    {
        return (
            mouseScreenPosition.x > startDownButton.x && mouseScreenPosition.x <= endDownButton.x &&
            mouseScreenPosition.y > startDownButton.y && mouseScreenPosition.y <= endDownButton.y);
    }

        public bool IsSwiping()
    {
        if (Direction != SwipeDirection.None)
            return true;
        else
            return false;
    }

    public void ShiftDown()
    {
        if (HexTile.activeHex != null)
            HexTile.activeHex.CommandHexDown();
    }

    public void ShiftLeft()
    {
        if (HexTile.activeHex != null)
        {

            HexTile.activeHex.CommandHexLeft();

        }
        else
        {
            if (cameraTarget.upperRowNeighbour != null)
                cameraTarget = HexNodeMap.instance.GetSpawnHex();

            cameraTarget = cameraTarget.antiClockwiseNeighbour;
        }
            
    }
    public void ShiftRight()
    {
        if (HexTile.activeHex != null)
        {
            HexTile.activeHex.CommandHexRight();
        }
        else
        {
            if (cameraTarget.upperRowNeighbour != null)
                cameraTarget = HexNodeMap.instance.GetSpawnHex();

            cameraTarget = cameraTarget.clockwiseNeighbour;
        }
    }

}
