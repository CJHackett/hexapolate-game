﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class RadioButtonHandler : MonoBehaviour
{
    [SerializeField] protected Image radioButton1;
    [SerializeField] protected Image radioButton2;

    public void RadioButton1Pressed()
    {
        radioButton2.enabled = false;
        radioButton1.enabled = true;
        Button1Action();
    }
    public void RadioButton2Pressed()
    {
        radioButton1.enabled = false;
        radioButton2.enabled = true;
        Button2Action();
    }

    protected abstract void Button1Action();
    protected abstract void Button2Action();
}
