﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelCycler : MonoBehaviour
{
    public GameObject[] panels;
    public Text panelNum;
    int currentPanel;
    
    void Start()
    {
        for(int i = 0; i < panels.Length; i++)
        {
            if (i == 0)
                panels[i].SetActive(true);
            else
                panels[i].SetActive(false);
        }
        currentPanel = 0;
        UpdateText();
    }

    public void MoveToNextPanel()
    {
        panels[currentPanel].SetActive(false);
        currentPanel++;
        if (currentPanel >= panels.Length)
            currentPanel = 0;

        panels[currentPanel].SetActive(true);
        UpdateText();
    }

    public void MoveToPrevPanel()
    {
        panels[currentPanel].SetActive(false);
        currentPanel--;
        if (currentPanel < 0)
            currentPanel = panels.Length-1;

        panels[currentPanel].SetActive(true);
        UpdateText();
    }

    void UpdateText()
    {
        if(panelNum!=null)
            panelNum.text = (currentPanel + 1) + " / " + panels.Length;
    }
}
