﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeSliderHandler : MonoBehaviour
{
    [SerializeField] Slider musicVolumeSlider;
    [SerializeField] Slider sfxVolumeSlider;

    private void Start()
    {
        UpdateMusicVolumeSlider();
        UpdateSfxVolumeSlider();
    }

    public void OnMusicVolumeSliderValueChanged()
    {
        AudioController.instance.SetMusicVolume(musicVolumeSlider.value);
        GameSettings.instance.MusicVolume = musicVolumeSlider.value;
        AudioController.instance.PlayClickUI();
    }
    public void OnSfxVolumeSliderValueChanged()
    {
        AudioController.instance.SetSfxVolume(sfxVolumeSlider.value);
        GameSettings.instance.SfxVolume = sfxVolumeSlider.value;
        AudioController.instance.PlayClickUI();
    }

    void UpdateMusicVolumeSlider()
    {
        musicVolumeSlider.value = GameSettings.instance.MusicVolume;
    }
    void UpdateSfxVolumeSlider()
    {
        sfxVolumeSlider.value = GameSettings.instance.SfxVolume;
    }
}
