﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathHighlighter : MonoBehaviour
{
    [SerializeField] GameObject highlightPrefab;
    Queue<GameObject> currPath = new Queue<GameObject>();

    //singleton
    public static PathHighlighter instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }

    public void HighlightPath(HexNode node)
    {
        if (GameSettings.instance.ShowPathHighlight && DifficultyController.currentLevel<=10)
        {
            UnHighlightPath();
            HexNode scanNode = node;
            while (!scanNode.lowerRowNeighbour.IsOccupied)
            {
                currPath.Enqueue(ObjectPool.Instance.ReuseObject(highlightPrefab, scanNode.lowerRowNeighbour.position, HexGenerator.instance.HexContainer.rotation));
                scanNode = scanNode.lowerRowNeighbour;
            }
        }
    }
    public void UnHighlightPath()
    {
        while (currPath.Count > 0)
            currPath.Dequeue().SetActive(false);
    }

    public void ToggleHighlighting()
    {
        if (GameSettings.instance.ShowPathHighlight)
            DisablePathHighlighting();
        else
            EnablePathHighlighting();
    }
    void DisablePathHighlighting()
    {
        GameSettings.instance.ShowPathHighlight = false;
        UnHighlightPath();
    }
    void EnablePathHighlighting()
    {
        GameSettings.instance.ShowPathHighlight = true;
        if (HexTile.activeHex != null)
        {
            HighlightPath(HexTile.activeHex.CurrNode);
        }
    }
}
