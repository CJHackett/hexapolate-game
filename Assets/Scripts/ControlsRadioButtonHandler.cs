﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsRadioButtonHandler : RadioButtonHandler
{
    private void Start()
    {
        if (GameSettings.instance.UseSwipeControls==true)
        {
            radioButton1.enabled = true;
            radioButton2.enabled = false;
        }
        else
        {
            radioButton2.enabled = true;
            radioButton1.enabled = false;
        }
    }

    //swipe
    protected override void Button1Action()
    {
        GameSettings.instance.UseSwipeControls = true;
        UIManager.Instance.HideControlPanel();
    }

    //buttons
    protected override void Button2Action()
    {
        GameSettings.instance.UseSwipeControls = false;
        UIManager.Instance.ShowControlPanel();
    }
}
