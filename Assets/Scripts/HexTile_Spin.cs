﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HexTile_Spin : HexTile
{
    [SerializeField] protected GameObject touchEffect;
    [SerializeField] protected Transform effectSpawnPoint;
    protected List<HexNode> connectedHexesInOrder;
    

    void Awake()
    {
        HexType = HexType.Spin;
        connectedHexesInOrder = new List<HexNode>();
    }

    //returns hexes in order of:
    // - bottom first, then clockwise
    protected void GetConnectedHexesInOrder()
    {
        connectedHexesInOrder.Clear();

        //get bottom hex
        if (CurrNode.lowerRowNeighbour.IsOccupied)
            connectedHexesInOrder.Add(CurrNode.lowerRowNeighbour);

        //if falls right on non flat edge, then get the lower left
        if (!CurrNode.fallsOnFlatEdge && CurrNode.fallsClockwise)
            if (CurrNode.lowerRowNeighbour.antiClockwiseNeighbour.IsOccupied)
                connectedHexesInOrder.Add(CurrNode.lowerRowNeighbour.antiClockwiseNeighbour);

        //get antiClockwiseNeighbour node
        if (CurrNode.antiClockwiseNeighbour.IsOccupied)
            connectedHexesInOrder.Add(CurrNode.antiClockwiseNeighbour);


        //if falls on flat edge, then get antiClockwiseNeighbour upper neighbour
        //get upper neighbours, 1 if flat edge, 2 if not flat edge (upper left first)
        if (CurrNode.fallsOnFlatEdge)
        {
            if (!CurrNode.antiClockwiseNeighbour.upperRowNeighbour.fallsOnFlatEdge)
            {
                if (CurrNode.antiClockwiseNeighbour.upperRowNeighbour.fallsClockwise)
                {
                    if (CurrNode.antiClockwiseNeighbour.upperRowNeighbour.clockwiseNeighbour.IsOccupied)
                        connectedHexesInOrder.Add(CurrNode.antiClockwiseNeighbour.upperRowNeighbour.clockwiseNeighbour);
                }
                else
                {
                    if (CurrNode.antiClockwiseNeighbour.upperRowNeighbour.IsOccupied)
                        connectedHexesInOrder.Add(CurrNode.antiClockwiseNeighbour.upperRowNeighbour);
                }
            }
            else
            {
                if (CurrNode.antiClockwiseNeighbour.upperRowNeighbour.IsOccupied)
                    connectedHexesInOrder.Add(CurrNode.antiClockwiseNeighbour.upperRowNeighbour);
            }

            if (CurrNode.upperRowNeighbour.IsOccupied)
                connectedHexesInOrder.Add(CurrNode.upperRowNeighbour);

            if (!CurrNode.clockwiseNeighbour.upperRowNeighbour.fallsOnFlatEdge)
            {
                if (!CurrNode.clockwiseNeighbour.upperRowNeighbour.fallsClockwise)
                {
                    if (CurrNode.clockwiseNeighbour.upperRowNeighbour.antiClockwiseNeighbour.IsOccupied)
                        connectedHexesInOrder.Add(CurrNode.clockwiseNeighbour.upperRowNeighbour.antiClockwiseNeighbour);
                }
                else
                {
                    if (CurrNode.clockwiseNeighbour.upperRowNeighbour.IsOccupied)
                        connectedHexesInOrder.Add(CurrNode.clockwiseNeighbour.upperRowNeighbour);
                }
            }
            else
            {
                if (CurrNode.clockwiseNeighbour.upperRowNeighbour.IsOccupied)
                    connectedHexesInOrder.Add(CurrNode.clockwiseNeighbour.upperRowNeighbour);
            }
        }
        else
        {
            if (CurrNode.upperRowNeighbour.fallsClockwise)
            {
                if (CurrNode.upperRowNeighbour.IsOccupied)
                    connectedHexesInOrder.Add(CurrNode.upperRowNeighbour);
                if (CurrNode.upperRowNeighbour.clockwiseNeighbour.IsOccupied)
                    connectedHexesInOrder.Add(CurrNode.upperRowNeighbour.clockwiseNeighbour);
            }
            else
            {
                if (CurrNode.upperRowNeighbour.antiClockwiseNeighbour.IsOccupied)
                    connectedHexesInOrder.Add(CurrNode.upperRowNeighbour.antiClockwiseNeighbour);
                if (CurrNode.upperRowNeighbour.IsOccupied)
                    connectedHexesInOrder.Add(CurrNode.upperRowNeighbour);
            }
        }

        //get clockwise neighbour
        if (CurrNode.clockwiseNeighbour.IsOccupied)
            connectedHexesInOrder.Add(CurrNode.clockwiseNeighbour);

        //if falls left on non flat edge, then get the lower right
        if (!CurrNode.fallsOnFlatEdge && !CurrNode.fallsClockwise)
            if (CurrNode.lowerRowNeighbour.clockwiseNeighbour.IsOccupied)
                connectedHexesInOrder.Add(CurrNode.lowerRowNeighbour.clockwiseNeighbour);
    }

}
