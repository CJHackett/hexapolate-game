﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile_SpinAntiClockwise : HexTile_Spin
{
    protected override void OnHexInitiated()
    {

    }

    protected override void OnHexFinishAttaching()
    {
        MarkForActioning(this);
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
        GetConnectedHexesInOrder();
        GameObject[] newHexTiles = new GameObject[connectedHexesInOrder.Count];

        int index;
        for (int i = 0; i < newHexTiles.Length; i++)
        {
            if (i == newHexTiles.Length - 1)
                index = 0;
            else
                index = i + 1;
            newHexTiles[i] = ObjectPool.Instance.GetPrefabFromInstanceID(connectedHexesInOrder[index].occupyingHexTile.gameObject.GetInstanceID());

            //return if connect type is center
            if (connectedHexesInOrder[index].occupyingHexTile.HexType == HexType.Center)
                return;
        }

        //swap hexes
        index = 0;
        foreach (HexNode node in connectedHexesInOrder)
        {
            node.occupyingHexTile.ReplaceHexTile(newHexTiles[index]);
            index++;
        }

        //debugList = connectedHexesInOrder.ToArray();
        MarkForRemoval(this);
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
        
    }

    protected override void OnHexDestroyed()
    {
        ObjectPool.Instance.ReuseObject(touchEffect, effectSpawnPoint.position, effectSpawnPoint.rotation);
    }
}
