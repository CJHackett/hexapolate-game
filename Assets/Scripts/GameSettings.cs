﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameDifficulty { Easy, Medium, Hard};

public class GameSettings : MonoBehaviour
{
    //audio settings
    float musicVolume;
    float musicVolume_default = -30f;
    float sfxVolume;
    float sfxVolume_default = -10f;
    bool muted;
    bool muted_default = false;

    //game settings
    bool showPathHighlight;
    bool showPathHighlight_default = true;
    float xSensitivity;
    float xSensitivity_default = 8f;
    float ySensitivity;
    float ySensitivity_default = 8f;
    bool useSwipeControls;
    bool useSwipeControls_default = true;
    int startLevel = 0;
    GameDifficulty difficulty;
    GameDifficulty difficulty_default = GameDifficulty.Easy;
    bool mediumUnlocked;
    bool mediumUnlocked_default = false;
    bool hardUnlocked;
    bool hardUnlocked_default = false;

    bool firstTimePlaying;

    [Header("Debug")]
    [SerializeField] bool debugEnabled = false;
    [SerializeField][Range(1,100)] int debugStartLevel = 1;

    public float MusicVolume { get => musicVolume; set { musicVolume = value; PlayerPrefs.SetFloat("musicVolume", musicVolume); } }
    public float SfxVolume { get => sfxVolume;  set { sfxVolume = value; PlayerPrefs.SetFloat("sfxVolume", sfxVolume); } }
    public bool Muted { get => muted; set { muted = value; SetPrefsBool("muted", muted); } }
    public bool ShowPathHighlight { get => showPathHighlight; set { showPathHighlight = value; SetPrefsBool("showPathHighlight", showPathHighlight); } }
    public bool UseSwipeControls{ get => useSwipeControls; set { useSwipeControls = value; SetPrefsBool("useSwipeControls", useSwipeControls); } }
    public float XSensitivity { get => xSensitivity; set { xSensitivity = value; PlayerPrefs.SetFloat("xSensitivity", xSensitivity); } }
    public float YSensitivity { get => ySensitivity; set { ySensitivity = value; PlayerPrefs.SetFloat("ySensitivity", ySensitivity); } }
    public GameDifficulty Difficulty { get => difficulty; set { difficulty = value; PlayerPrefs.SetInt("difficulty", (int)difficulty); } }
    public bool MediumUnlocked { get => mediumUnlocked; set { mediumUnlocked = value; SetPrefsBool("mediumUnlocked", mediumUnlocked); } }
    public bool HardUnlocked { get => hardUnlocked; set { hardUnlocked = value; SetPrefsBool("hardUnlocked", hardUnlocked); } }
    public bool FirstTimePlaying { get => firstTimePlaying; set { firstTimePlaying = value; SetPrefsBool("firstTimePlaying", firstTimePlaying); } }
    public int StartLevel { get => startLevel; set => startLevel = value; }
    public bool DebugEnabled { get => debugEnabled; }

    public static GameSettings instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
        GetSettingsFromPrefs();

        if (debugEnabled == true)
            startLevel = debugStartLevel-1;
        else
            startLevel = 0;
    }

    void GetSettingsFromPrefs()
    {
        musicVolume = PlayerPrefs.GetFloat("musicVolume", musicVolume_default);
        sfxVolume = PlayerPrefs.GetFloat("sfxVolume", sfxVolume_default);
        muted = GetPrefsBool("muted", muted_default);
        showPathHighlight = GetPrefsBool("showPathHighlight", showPathHighlight_default);
        xSensitivity = PlayerPrefs.GetFloat("xSensitivity", xSensitivity_default);
        ySensitivity = PlayerPrefs.GetFloat("ySensitivity", ySensitivity_default);
        difficulty = (GameDifficulty)PlayerPrefs.GetInt("difficulty", (int)difficulty_default);
        useSwipeControls = GetPrefsBool("useSwipeControls", useSwipeControls_default);
        mediumUnlocked = GetPrefsBool("mediumUnlocked", mediumUnlocked_default);
        hardUnlocked = GetPrefsBool("hardUnlocked", hardUnlocked_default);
        firstTimePlaying = GetPrefsBool("firstTimePlaying", true);
    }

    bool GetPrefsBool(string pref, bool defaultValue)
    {
        if (defaultValue)
        {
            if (PlayerPrefs.GetFloat(pref, 1) > 0)
                return true;
            else return false;
        }
        else
        {
            if (PlayerPrefs.GetFloat(pref, 0) > 0)
                return true;
            else return false;
        }
    }
    void SetPrefsBool(string pref, bool value)
    {
        if (value)
            PlayerPrefs.SetFloat(pref, 1);
        else
            PlayerPrefs.SetFloat(pref, 0);
    }
}
