﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SensitivitySliderHandler : MonoBehaviour
{
    [SerializeField] Slider xSensitivitySlider;
    [SerializeField] Slider ySensitivitySlider;
    [SerializeField] PlayerInput playerInput;

    private void Start()
    {
        UpdateSliderX();
        UpdateSliderY();
    }

    public void OnXSliderValueChanged()
    {
        playerInput.xSensitivity = xSensitivitySlider.value;
        GameSettings.instance.XSensitivity = xSensitivitySlider.value;
        AudioController.instance.PlayClickUI();
    }
    public void OnYSliderValueChanged()
    {
        playerInput.ySensitivity = ySensitivitySlider.value;
        GameSettings.instance.YSensitivity = ySensitivitySlider.value;
        AudioController.instance.PlayClickUI();
    }

    void UpdateSliderX()
    {
        xSensitivitySlider.value = GameSettings.instance.XSensitivity;
    }
    void UpdateSliderY()
    {
        ySensitivitySlider.value = GameSettings.instance.YSensitivity;
    }
}
