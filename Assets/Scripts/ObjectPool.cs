﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public PoolObject[] poolOptions;
    Dictionary<int, Queue<ObjectInstance>> poolDictionary = new Dictionary<int, Queue<ObjectInstance>>();
    Dictionary<int, PoolObject> poolOptionRecord = new Dictionary<int, PoolObject>();
    Dictionary<int, GameObject> instanceDictionary = new Dictionary<int, GameObject>();

    public static ObjectPool Instance { get { return _instance; } }
    static ObjectPool _instance = null;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            foreach (PoolObject pool in poolOptions)
                CreatePool(pool);
        }
        else
        {
            Destroy(this);
        }
    }

    public void CreatePool(PoolObject options)
    {
        int poolKey = options.prefab.GetInstanceID();
        poolOptionRecord.Add(poolKey, options);

        if (!poolDictionary.ContainsKey(poolKey))
        {
            poolDictionary.Add(poolKey, new Queue<ObjectInstance>());

            for (int i = 0; i < options.poolSize; i++)
            {
                AddItemToPool(options, poolKey);
            }
        }
    }

    ObjectInstance AddItemToPool(PoolObject options, int poolKey)
    {
        ObjectInstance newObject = new ObjectInstance(Instantiate(options.prefab) as GameObject);
        poolDictionary[poolKey].Enqueue(newObject);
        if (options.hasParent)
            newObject.gameObject.transform.SetParent(options.parentObject);
        newObject.gameObject.SetActive(false);
        instanceDictionary.Add(newObject.gameObject.GetInstanceID(), options.prefab);
        return newObject;
    }


    public GameObject ReuseObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        int poolKey = prefab.GetInstanceID();

        if (poolDictionary.ContainsKey(poolKey))
        {
            ObjectInstance objectToReuse = poolDictionary[poolKey].Dequeue();
            poolDictionary[poolKey].Enqueue(objectToReuse);

            //expands pool if required
            if (objectToReuse.gameObject.activeInHierarchy)
                objectToReuse = AddItemToPool(poolOptionRecord[prefab.GetInstanceID()], prefab.GetInstanceID());

            objectToReuse.Reuse(position, rotation);

            return objectToReuse.gameObject;
        }
        else
            return null;
    }

    public GameObject GetPrefabFromInstanceID(int objectID)
    {
        return instanceDictionary[objectID];
    }

    public class ObjectInstance
    {

        public GameObject gameObject;
        Transform transform;

        bool hasPoolObjectComponent;
        PooledObject poolObjectScript;

        public ObjectInstance(GameObject objectInstance)
        {
            gameObject = objectInstance;
            transform = gameObject.transform;

            if (gameObject.GetComponentInChildren<PooledObject>())
            {
                hasPoolObjectComponent = true;
                poolObjectScript = gameObject.GetComponentInChildren<PooledObject>();
            }
            else
                hasPoolObjectComponent = false;
        }

        public void Reuse(Vector3 position, Quaternion rotation)
        {
            gameObject.SetActive(true);
            transform.position = position;
            transform.rotation = rotation;

            if (hasPoolObjectComponent)
            {
                poolObjectScript.OnObjectReuse();
            }
        }
    }
}

[System.Serializable]
public struct PoolObject
{
    public GameObject prefab;
    public int poolSize;
    public bool hasParent;
    public Transform parentObject;
}

public abstract class PooledObject : MonoBehaviour
{

    public abstract void OnObjectReuse();

    public void ReturnToPool()
    {
        gameObject.SetActive(false);
    }
}
