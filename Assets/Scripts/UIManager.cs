﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;

public class UIManager : MonoBehaviour {

    public GameObject pausePanel;
    public GameObject optionsPanel;
    [Space]
    public GameObject gameOverPanel;
    public Text newHighScoreText;
    [Space]
    public GameObject winGamePanel;
    [Space]
    public GameObject controlButtonsPanel;
    [Space]
    public CanvasGroup fadeImage;
    public GameObject fadePanelObject;
    public float fadeSpeed;
    [Space]
    AudioController audioController;
    public Image muteButtonImage;
    [Space]
    public GameObject scoreLevelPanel;
    public GameObject nextHexPanel;

    float savedDeltaTime;
    bool fadeComplete;

    float backButtonDelay = 0.2f;
    float timeSinceLastBackPress;

    public static UIManager Instance { get { return _instance; } }
    static UIManager _instance = null;


    private void Awake()
    {
        if (Instance == null)
        {
            _instance = this;
            pausePanel.SetActive(false);
            fadeComplete = false;
            optionsPanel.SetActive(false);
            gameOverPanel.SetActive(false);
            fadePanelObject.SetActive(true);
            winGamePanel.SetActive(false);
            audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
            timeSinceLastBackPress = Time.unscaledTime;
        }
        else
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        audioController.RefreshMuteButton(muteButtonImage);
        if (GameSettings.instance.UseSwipeControls)
            HideControlPanel();
        else
            ShowControlPanel();
        ShowScoreLevelPanel();
    }

    private void Update()
    {
        if (Time.unscaledTime > timeSinceLastBackPress + backButtonDelay)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                if (pausePanel.activeInHierarchy != true)
                    OpenPausePanel();
                else
                    ClosePausePanel();
                timeSinceLastBackPress = Time.unscaledTime;
            }
        }
    }

    public void HideControlPanel()
    {
        controlButtonsPanel.SetActive(false);
    }
    public void ShowControlPanel()
    {
        controlButtonsPanel.SetActive(true);
    }

    public void MuteButtonPressed()
    {
        if (audioController.IsMuted())
        {
            audioController.UnMute(muteButtonImage);
            AudioController.instance.PlayClickUI();
        }
        else
            audioController.Mute(muteButtonImage);
    }

    public IEnumerator FadeInCamera(Action callback)
    {
        yield return new WaitForSeconds(1f);
        if (fadeImage != null)
        {
            float fadePerFrame = fadeSpeed * Time.deltaTime;
            float alpha = 1;
            fadeImage.alpha = alpha;
            while (alpha > 0)
            {
                yield return null;
                alpha -= fadePerFrame;
                if (alpha < 0)
                    alpha = 0;
                fadeImage.alpha = alpha;
            }
            fadePanelObject.SetActive(false);
            fadeImage.alpha = 1;
        }
        fadeComplete = true;
        callback();
    }

    public void OpenPausePanel()
    {
        if (gameOverPanel.activeInHierarchy || winGamePanel.activeInHierarchy || pausePanel.activeInHierarchy || optionsPanel.activeInHierarchy || fadeComplete == false)
            return;

        savedDeltaTime = Time.timeScale;
        Time.timeScale = 0.0f;
        AudioController.instance.PlayClickUI();
        AudioController.instance.PauseSfx();
        pausePanel.SetActive(true);
    }

    public void OpenGameOverPanel()
    {
        if (winGamePanel.activeInHierarchy)
            return;

        //report score to leaderboard
        if (Social.localUser.authenticated)
            Social.ReportScore(PlayerScore.instance.CurrentScore, GPGSIds.leaderboard_top_stars, (bool success) => {});

        //report score to local leaderboard
        if (PlayerScore.instance.CurrentScore > PlayerPrefs.GetInt("HighScore",0))
        {
            PlayerPrefs.SetInt("HighScore", PlayerScore.instance.CurrentScore);
            newHighScoreText.text = "NEW HIGH SCORE!";
        }
        else
        {
            newHighScoreText.text = " ";
        }

        savedDeltaTime = Time.timeScale;
        gameOverPanel.SetActive(true);
    }
    public void OpenGameWinPanel()
    {
        if (gameOverPanel.activeInHierarchy)
            return;

        savedDeltaTime = Time.timeScale;
        winGamePanel.SetActive(true);
    }

    public void ReturnToMainMenu()
    {
        AudioController.instance.PlayClickUI();
        Time.timeScale = savedDeltaTime;
        AudioController.instance.sfxSource.Stop();
        SceneManager.LoadScene("Menu");
    }

    public void OpenOptionsPanel()
    {
        AudioController.instance.PlayClickUI();
        optionsPanel.SetActive(true);
        pausePanel.SetActive(false);
        HideScoreLevelPanel();
    }
    public void CloseOptionsPanel()
    {
        AudioController.instance.PlayClickUI();
        optionsPanel.SetActive(false);
        pausePanel.SetActive(true);
        ShowScoreLevelPanel();
    }

    public void ClosePausePanel()
    {
        AudioController.instance.PlayClickUI();
        pausePanel.SetActive(false);
        Time.timeScale = savedDeltaTime;
        AudioController.instance.UnPauseSfx();
    }

    private void OnApplicationFocus(bool focus)
    {
        if(!focus)
            OpenPausePanel();   
    }

    public void HideScoreLevelPanel()
    {
        scoreLevelPanel.SetActive(false);
        nextHexPanel.SetActive(false);
    }

    public void ShowScoreLevelPanel()
    {
        scoreLevelPanel.SetActive(true);
        nextHexPanel.SetActive(true);
    }

}
