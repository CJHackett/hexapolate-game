﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HexTileColour { Red, Teal, Purple, Orange, Pink, Green }

public class HexTile_Plain : HexTile
{
    [SerializeField] HexTileColour tileColour;
    public HexTileColour TileColour { get => tileColour;}
    
    [SerializeField] int hexChainBreakLimit;
    List<HexTile_Plain> hexChain;
    
    public HexTile_Plain HexChainHead { get; set; }
    public List<HexTile_Plain> HexChain { get => HexChainHead.hexChain; set => HexChainHead.hexChain = value; }

    [SerializeField] bool drawGizmos = false;

    const int destructionScore = 10;

    protected virtual void Awake()
    {
        HexType = HexType.Plain;
        hexChain = new List<HexTile_Plain>();
    }

    protected override void OnHexInitiated()
    {
        ResetChain();
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {     
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
        if (HexType == otherHex.HexType)
        {
            HexTile_Plain otherColourTile = otherHex as HexTile_Plain;
            if (AreHexesOnSameChain(this, otherColourTile))
                DirtyChain();
            else
                otherColourTile.DirtyChain();
        }
    }

    protected override void OnHexFinishAttaching()
    {
        HexTile[] otherHexes = attachedHexTiles.ToArray();
        for (int i = 0; i < otherHexes.Length; i++)
        {
            if (HexType == otherHexes[i].HexType)
            {
                HexTile_Plain otherColourTile = otherHexes[i] as HexTile_Plain;
                if (otherColourTile.TileColour == TileColour && !AreHexesOnSameChain(this, otherColourTile))
                {
                    Debug.Log("Hexes creating chain: [" + this + "] and [" + otherHexes[i] + "]");
                    CombineChains(otherColourTile);
                }
            }
        }

        if (HexChain.Count >= hexChainBreakLimit)
        {
            PlayerAchievements.instance.ReportChainLength(HexChain.Count);
            MarkForActioning(HexChainHead);
        }
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
        if (HexChain.Count >= hexChainBreakLimit)
            HexChainHead.DestroyChain();
    }


    protected override void OnHexDestroyed()
    {
        PlayerScore.instance.UpdateScore(destructionScore * currentMultiplier);
    }


    //Chain management methods
    public void DestroyChain()
    {
        HexTile_Plain[] hexesToDestroy = HexChain.ToArray();
        for (int i = 0; i < hexesToDestroy.Length; i++)
        {
            MarkForRemoval(hexesToDestroy[i]);
            hexesToDestroy[i].CanDamageBarrier = true;
            hexesToDestroy[i].ResetChain();
        }
    }

    void CombineChains(HexTile_Plain other)
    {
        if (HexChain.Count >= other.HexChain.Count)
        {
            HexTile_Plain[] hexesToRehead = other.HexChain.ToArray();
            for (int i = 0; i < hexesToRehead.Length; i++)
                hexesToRehead[i].AddHexToOtherHexChain(this);
        }
        else
        {
            HexTile_Plain[] hexesToRehead = HexChain.ToArray();
            for (int i = 0; i < hexesToRehead.Length; i++)
                hexesToRehead[i].AddHexToOtherHexChain(other);
        }
        //Debug.Log("Chain length: " + HexChain.Count);
    }


    //splits the chain back up into individual links to be reattached at the end of the turn
    void DirtyChain()
    {
        foreach (HexTile_Plain hex in HexChain.ToArray())
        {
            hex.ResetChain();
            MarkForChecking(hex);
        }
    }
    public void AddHexToOtherHexChain(HexTile_Plain otherHex)
    {
        if (IsHead)
            HexChain.Clear();

        otherHex.HexChain.Add(this);
        HexChainHead = otherHex.HexChainHead;
    }
    public void ResetChain()
    {
        HexChainHead = this;
        hexChain.Clear();
        HexChain.Add(this);
    }

    //quick check methods
    public bool IsHead
    {
        get
        {
            if (HexChainHead == this)
                return true;
            else
                return false;
        }
    }

    

    public static bool AreHexesOnSameChain(HexTile_Plain hex1, HexTile_Plain hex2)
    {
        if (hex1.HexChainHead == hex2.HexChainHead)
            return true;
        else
            return false;
    }

    private void OnDrawGizmos()
    {
        if (drawGizmos && HexChainHead != null)
        {
            if(IsHead){
                foreach (HexTile_Plain hex in HexChain)
                {
                    if (hex.IsHead)
                    {
                        Gizmos.color = Color.blue;
                    }
                    else
                    {
                        Gizmos.color = Color.red;
                    }
                    Gizmos.DrawSphere(hex.transform.position, 0.5f);  
                }
            }
        }
    }
}
