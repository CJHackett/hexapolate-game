﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PingPongTextAlpha : MonoBehaviour {

    public Text target;
    public float speed = 1f;

    private float _t = 0f;
    public float startDelay = 0;
    [Range(0,1)]public float startAlpha=1;
    float startTime_sec;

    private void Awake()
    {
        startTime_sec = Time.time + startDelay;
        target.color = new Color(target.color.r, target.color.g, target.color.b, startAlpha);
    }

    void Update()
    {
        if (Time.time > startTime_sec)
        {
            _t += Time.deltaTime * speed;
            target.color = new Color(target.color.r, target.color.g, target.color.b, Mathf.PingPong(_t, 1f));
        }
    }
}
