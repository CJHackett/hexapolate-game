﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SensitivityButtonHandler : MonoBehaviour
{
    const int sliderMax = 10;
    const int sliderMin = 1;

    [SerializeField] Text horiSenText;
    [SerializeField] Text vertSenText;

    int horiSenValue;
    int vertSenValue;

    private void Start()
    {
        horiSenValue = ConvertFromPlayerInput(GameSettings.instance.XSensitivity);
        vertSenValue = ConvertFromPlayerInput(GameSettings.instance.YSensitivity);
        UpdateHorizontalSensitivityText();
        UpdateVerticalSensitivityText();
    }

    public void HorizonatalSensitivityIncrease()
    {
        if (horiSenValue < sliderMax)
        {
            horiSenValue++;
            PlayerInput.Instance.xSensitivity = ConvertToPlayerInput(horiSenValue);
            GameSettings.instance.XSensitivity = (ConvertToPlayerInput(horiSenValue));
            AudioController.instance.PlayClickUI();
            UpdateHorizontalSensitivityText();
        }
    }
    public void HorizonatalSensitivityDecrease()
    {
        if (horiSenValue > sliderMin)
        {
            horiSenValue--;

            PlayerInput.Instance.xSensitivity = ConvertToPlayerInput(horiSenValue);
            GameSettings.instance.XSensitivity = (ConvertToPlayerInput(horiSenValue));
            AudioController.instance.PlayClickUI();
            UpdateHorizontalSensitivityText();
        }
    }


    public void VerticalSensitivityIncrease()
    {
        if (vertSenValue < sliderMax)
        {
            vertSenValue++;

            PlayerInput.Instance.ySensitivity = ConvertToPlayerInput(vertSenValue);
            GameSettings.instance.YSensitivity = (ConvertToPlayerInput(vertSenValue));
            AudioController.instance.PlayClickUI();
            UpdateVerticalSensitivityText();
        }
    }
    public void VerticalSensitivityDecrease()
    {
        if (vertSenValue > sliderMin)
        {
            vertSenValue--;

            PlayerInput.Instance.ySensitivity = ConvertToPlayerInput(vertSenValue);
            GameSettings.instance.YSensitivity = (ConvertToPlayerInput(vertSenValue));
            AudioController.instance.PlayClickUI();
            UpdateVerticalSensitivityText();
        }
    }

    float ConvertToPlayerInput(int value)
    {
        float currFraction = Mathf.InverseLerp(sliderMin, sliderMax, value);
        return Mathf.Lerp(PlayerInput.minSensitivity, PlayerInput.maxSensitivity, currFraction);

    }
    int ConvertFromPlayerInput(float value)
    {
        float senFraction = Mathf.InverseLerp(PlayerInput.minSensitivity, PlayerInput.maxSensitivity, value);
        return (int)(Mathf.Lerp(sliderMin, sliderMax, senFraction));
    }

    void UpdateHorizontalSensitivityText()
    {
        horiSenText.text = horiSenValue.ToString();

    }
    void UpdateVerticalSensitivityText()
    {
        vertSenText.text = vertSenValue.ToString();
    }
}
