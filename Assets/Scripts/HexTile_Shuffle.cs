﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile_Shuffle : HexTile
{
    [SerializeField] protected GameObject touchEffect;
    [SerializeField] protected Transform effectSpawnPoint;

    private void Awake()
    {
        HexType = HexType.Shuffle;
    }

    protected override void OnHexInitiated()
    {

    }

    protected override void OnHexFinishAttaching()
    {
        MarkForActioning(this);
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
        HexNode[] currentHexNodes = new HexNode[attachedHexTiles.Count];
        GameObject[] newHexTiles = new GameObject[currentHexNodes.Length];

        for (int i = 0; i < currentHexNodes.Length; i++)
        {
            currentHexNodes[i] = attachedHexTiles[i].CurrNode;
            newHexTiles[i] = ObjectPool.Instance.GetPrefabFromInstanceID(attachedHexTiles[i].gameObject.GetInstanceID());

            //return if connect type is center
            if (attachedHexTiles[i].HexType == HexType.Center)
                return;
        }

        //shuffle the objects
        GameObject tempObject;
        for (int i = 1; i < newHexTiles.Length; i++)
        {
            int rnd = Random.Range(0, newHexTiles.Length);
            tempObject = newHexTiles[rnd];
            newHexTiles[rnd] = newHexTiles[i];
            newHexTiles[i] = tempObject;
        }

        //replace hexes with the new shuffled set
        for (int i = 0; i < currentHexNodes.Length; i++)
            currentHexNodes[i].occupyingHexTile.ReplaceHexTile(newHexTiles[i]);
        
        MarkForRemoval(this);
    
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
    }

    protected override void OnHexDestroyed()
    {
        ObjectPool.Instance.ReuseObject(touchEffect, effectSpawnPoint.position, effectSpawnPoint.rotation);
    }
}
