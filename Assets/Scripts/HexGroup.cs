﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexGroup : MonoBehaviour
{
    List<HexTile> attachedTiles = new List<HexTile>();

    void CreateNewGroup(HexNode startNode, int groupSize)
    {
        attachedTiles.Clear();


    }

    public void MoveGroupRight()
    {
        foreach(HexTile hex in attachedTiles)
        {
            hex.CommandHexRight();
        }
    }

    public void MoveGroupLeft()
    {
        foreach (HexTile hex in attachedTiles)
        {
            hex.CommandHexLeft();
        }
    }
    
    public void MoveGroupDown()
    {
        foreach (HexTile hex in attachedTiles)
        {
            hex.CommandHexDown();
        }
    }

    public void DispandGroup()
    {

    }

}
