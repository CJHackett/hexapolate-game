﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HexGenerator : MonoBehaviour
{
    [SerializeField] GameObject centerHex;
    [SerializeField] GameObject barrierHex;
    [SerializeField] GameObject deadHex;
    [SerializeField] GameObject bombHex;
    [SerializeField] GameObject rainbowHex;
    [SerializeField] GameObject randomHex;
    [SerializeField] GameObject shuffleHex;
    [SerializeField] GameObject spinHex_clockwise;
    [SerializeField] GameObject spinHex_anticlockwise;
    [SerializeField] GameObject[] plainHex;
    [SerializeField] Transform hexContainer;
    [SerializeField] float hexSpawnDelay_sec;
    [SerializeField] float firstHexSpawnDelay_sec;
    [SerializeField] GameObject barrierSpawnEffect;
    WaitForSeconds hexSpawnDelay;
    WaitForSeconds setupHexesDelay = new WaitForSeconds(0.05f);
    float rainbowDropChance = 5f;
    float bombDropChance = 3f;
    float randomDropChance = 2f;
    float spinAntiDropChance = 1f;
    float spinClkDropChance = 1f;
    float shuffleDropChance = 1f;
    int availableColours;
    public int AvailableColours { get => availableColours; }
    public Transform HexContainer { get => hexContainer;}
    public GameObject[] PlainHex { get => plainHex; private set => plainHex = value; }

    [Header("Next Hex Indicators")]
    [SerializeField] Transform nextHexIndicator;
    HexTile nextHex;
    GameObject nextHexGameObject;

    //global flag to indicate the hexes should be spawned is running
    public static bool dropHexes_flag;

    //singleton
    public static HexGenerator instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        availableColours = plainHex.Length;
        nextHex = null;

        //shuffle random colours
        GameObject tempObject;
        for (int i = 1; i < plainHex.Length; i++)
        {
            int rnd = Random.Range(0, plainHex.Length);
            tempObject = plainHex[rnd];
            plainHex[rnd] = plainHex[i];
            plainHex[i] = tempObject;
        }
    }

    private void Start()
    {
        //checks the hex node map has been created
        if (HexNodeMap.instance == null)
            Debug.Log("Error: No HexNode map generated");
        else
        {
            if (UIManager.Instance != null)
                UIManager.Instance.StartCoroutine(UIManager.Instance.FadeInCamera(StartGame));
            else
                StartGame();
        }
    
    }

    //Called from within the class as a callback once the sceen has faded into view
    void StartGame()
    {
        dropHexes_flag = false;
        HexTile.ResetAllStaticLists();

        CreateCenterHex();

        StartCoroutine(CreateBarrierHexes());
    }

    //called exteranlly to respawn the current tiles
    public void ResetBarrierTiles()
    {
        StartCoroutine(CreateBarrierHexes());
    }

    void CreateCenterHex()
    {
        SpawnHex(centerHex, HexNodeMap.instance.GetHexRing(0)[0], false);
    }

    IEnumerator CreateBarrierHexes()
    {
        if (nextHex != null)
        {
            nextHex.gameObject.SetActive(false);
            nextHex = null;
        }

        //wait until difficulty has been loaded before continuing
        while (!DifficultyController.difficultyLoaded)
            yield return null;

        for (int i = 0; i < DifficultyController.currNumRows; i++)
        {
            List<HexNode> hexes = HexNodeMap.instance.GetHexRing(i + 1);
            if (hexes != null)
            {
                foreach (HexNode hex in hexes)
                {
                    yield return setupHexesDelay;
                    SpawnHex(barrierHex, hex, false);
                    ObjectPool.Instance.ReuseObject(barrierSpawnEffect, new Vector3(hex.position.x, hex.position.y, hex.position.z+1f), barrierSpawnEffect.transform.rotation);
                }
            }
        }

        PlayerAchievements.instance.MarkGameStart();

        //create barrier hexes around the center tile
        StartCoroutine(HexSpawnerCoroutine());
    }

    GameObject PickRandomHex()
    {
        //spawns either a dead hex, coloured hex, or bomb hex
        GameObject hexToSpawn;
        float randomValue = Random.value * 100;
        if (randomValue < DifficultyController.currDeadDropChance)
            hexToSpawn = deadHex;
        else if (randomValue < DifficultyController.currDeadDropChance + bombDropChance)
            hexToSpawn = bombHex;
        else if (randomValue < (DifficultyController.currDeadDropChance + bombDropChance + rainbowDropChance))
            hexToSpawn = rainbowHex;
        else if (randomValue < (DifficultyController.currDeadDropChance + bombDropChance + rainbowDropChance + randomDropChance))
            hexToSpawn = randomHex;
        else if (randomValue < (DifficultyController.currDeadDropChance + bombDropChance + rainbowDropChance + randomDropChance + spinAntiDropChance))
            hexToSpawn = spinHex_anticlockwise;
        else if (randomValue < (DifficultyController.currDeadDropChance + bombDropChance + rainbowDropChance + randomDropChance + spinAntiDropChance + spinClkDropChance))
            hexToSpawn = spinHex_clockwise;
        else if (randomValue < (DifficultyController.currDeadDropChance + bombDropChance + rainbowDropChance + randomDropChance + spinAntiDropChance + spinClkDropChance + shuffleDropChance))
            hexToSpawn = shuffleHex;
        else
            hexToSpawn = plainHex[Random.Range(0, DifficultyController.currColours)];

        return hexToSpawn;
    }

    IEnumerator HexSpawnerCoroutine()
    {
        //delay before first hex drop
        dropHexes_flag = true;
        PrepareNextHex();

        yield return new WaitForSeconds(firstHexSpawnDelay_sec);

        //spawn tile then wait until it is no longer active
        while (dropHexes_flag)
        {
            //if cannot spawn hex, then node must be occupied so game is over
            if (SpawnNextHex(HexNodeMap.instance.GetSpawnHex(), true))
            {
                PrepareNextHex();
                while (HexTile.hexesAreActive)
                {
                    yield return null;
                }
                yield return hexSpawnDelay;
            }
            else
            {
                nextHex = null;
                StopGeneratingHexes();

                PlayerAchievements.instance.BankScores();
                UIManager.Instance.OpenGameOverPanel();
            }
        }
    }

    public static void StopGeneratingHexes()
    {
        dropHexes_flag = false;
    }

    public void PrepareNextHex()
    {
        GameObject nextHexObject = PickRandomHex();
        nextHex = InstantiateHex(nextHexObject, nextHexIndicator.position);
    }

    //returns true if hex has spawned successfully
    bool SpawnNextHex(HexNode node, bool startHexFalling)
    {
        //if occupied already then game is over
        if (node.IsOccupied)
            return false;

        nextHex.InitHexTile(node, startHexFalling);

        return true;
    }
    bool SpawnHex(GameObject objectToSpawn, HexNode node, bool startHexFalling)
    {
        //if occupied already then game is over
        if (node.IsOccupied)
            return false;

        HexTile spawnedTile = InstantiateHex(objectToSpawn, node.position);
        spawnedTile.InitHexTile(node, startHexFalling);

        return true;
    }

    public HexTile InstantiateHex(GameObject objectToSpawn, Vector3 position)
    {
        return ObjectPool.Instance.ReuseObject(objectToSpawn, position, hexContainer.rotation).GetComponent<HexTile>();
    }

    
}