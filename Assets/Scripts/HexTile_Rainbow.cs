﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile_Rainbow : HexTile
{
    [SerializeField] GameObject touchEffect;
    [SerializeField] Transform effectSpawnPoint;

    void Awake()
    {
        HexType = HexType.Rainbow;
    }

    protected override void OnHexInitiated()
    {

    }

    protected override void OnHexFinishAttaching()
    {
        HexType typeToMimic = HexType.Dead;
        GameObject objectToMimic = null;
        int largestChainSoFar = 0;
        for(int i=0;i<attachedHexTiles.Count;i++)
        {
            switch (attachedHexTiles[i].HexType)
            {
                case HexType.Barrier:
                    //if not assigned or dead hex selected, pick barrier
                    if (objectToMimic == null)
                    {
                        objectToMimic = attachedHexTiles[i].gameObject;
                        typeToMimic = HexType.Barrier;
                    }
                    else
                    {
                        if (typeToMimic == HexType.Dead)
                        {
                            objectToMimic = attachedHexTiles[i].gameObject;
                            typeToMimic = HexType.Barrier;
                        }
                    }
                    break;
                case HexType.Plain:
                    HexTile_Plain randomHex = attachedHexTiles[i] as HexTile_Plain;
                    if (objectToMimic == null)
                    {
                        objectToMimic = attachedHexTiles[i].gameObject;
                        typeToMimic = HexType.Plain;
                        largestChainSoFar = randomHex.HexChain.Count;
                    }
                    else
                    {
                        if (typeToMimic != HexType.Plain)
                        {
                            objectToMimic = attachedHexTiles[i].gameObject;
                            typeToMimic = HexType.Plain;
                            largestChainSoFar = randomHex.HexChain.Count;
                        }
                        else
                        {
                            if (randomHex.HexChain.Count > largestChainSoFar)
                            {
                                objectToMimic = attachedHexTiles[i].gameObject;
                                largestChainSoFar = randomHex.HexChain.Count;
                            }
                        }
                    }
                    break;
                case HexType.Dead:
                    if (objectToMimic == null)
                    {
                        objectToMimic = attachedHexTiles[i].gameObject;
                        typeToMimic = HexType.Dead;
                    }
                    break;
                case HexType.Center:
                    return;
                default:
                    return;
            }
        }

        if (objectToMimic != null)
        {
            ObjectPool.Instance.ReuseObject(touchEffect, new Vector3(CurrNode.position.x, CurrNode.position.y, effectSpawnPoint.position.z), effectSpawnPoint.rotation);

            ReplaceHexTile(ObjectPool.Instance.GetPrefabFromInstanceID(objectToMimic.GetInstanceID()));
        }
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
    }

    protected override void OnHexDestroyed()
    {
    }
}
