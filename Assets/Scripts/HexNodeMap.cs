﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexNodeMap : MonoBehaviour
{
    //singleton
    public static HexNodeMap instance = null;

    //map variables
    [SerializeField][Range(1,15)] int mapSize;
    List<List<HexNode>> hexNodes;
    HexNode[] outerHexNodes;
    public const float outerRadius = 1f;
    public const float innerRadius = outerRadius * 0.866025404f;

    //debug
    [Header("Debug")]
    [SerializeField] bool debugFallDirection = false;
    [SerializeField] bool debugUpperRow = false;
    [SerializeField] bool debugOccupied = false;
    [SerializeField] Mesh hexGizmoMesh;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        GenerateHexNodeMap();
    }

    void GenerateHexNodeMap()
    {
        //generate the hex nodes array
        int numOfNodes = (6 * (mapSize - 1)) + 1;
        hexNodes = new List<List<HexNode>>();
        Vector3 origin = Vector3.zero;

        //set the node positions
        for (int hexRingIndex = 0; hexRingIndex < mapSize; hexRingIndex++)
        {
            hexNodes.Add(GenerateHexRing(hexRingIndex, origin));
        }
        outerHexNodes = hexNodes[hexNodes.Count-1].ToArray();

        //save the lower neighbours to the hex node structures
        int ringCount = 0;
        foreach(List<HexNode> hexRing in hexNodes)
        {
            //ignore the center hex
            if (ringCount != 0)
            {
                //special case for first ring index
                if (ringCount == 1)
                {
                    foreach (HexNode hex in hexRing)
                        hex.lowerRowNeighbour = hexNodes[0][0];
                }
                else
                {
                    int i = 0;
                    //first node neighours directly below
                    hexNodes[ringCount][i].lowerRowNeighbour = hexNodes[ringCount - 1][0];
                    hexNodes[ringCount][i].lowerRowNeighbour.upperRowNeighbour = hexNodes[ringCount][i];
                    i++;
                    //all but last side
                    for (int offset = 1; offset < 6; offset++)
                    {
                        for (int j = 0; j < ringCount; j++)
                        {
                            if (j == ringCount - 1)
                            {
                                hexNodes[ringCount][i].lowerRowNeighbour = hexNodes[ringCount - 1][i - offset];
                                hexNodes[ringCount][i].lowerRowNeighbour.upperRowNeighbour = hexNodes[ringCount][i];
                            }
                            else
                            {
                                if (IsDecentNodeLeft(hexNodes[ringCount][i], hexNodes[ringCount - 1][i - offset], hexNodes[ringCount - 1][i - offset + 1], origin))
                                {
                                    hexNodes[ringCount][i].lowerRowNeighbour = hexNodes[ringCount - 1][i - offset];
                                    hexNodes[ringCount][i].lowerRowNeighbour.upperRowNeighbour = hexNodes[ringCount][i];
                                }
                                else
                                {
                                    hexNodes[ringCount][i].lowerRowNeighbour = hexNodes[ringCount - 1][i - (offset - 1)];
                                    hexNodes[ringCount][i].fallsClockwise = true;
                                    hexNodes[ringCount][i].lowerRowNeighbour.upperRowNeighbour = hexNodes[ringCount][i];
                                }
                            }
                            i++;
                        }
                    }
                    //last side
                    for (int j = 0; j < ringCount-1; j++)
                    {
                        if (j == ringCount - 2)
                        {
                            hexNodes[ringCount][i].lowerRowNeighbour = hexNodes[ringCount - 1][i - 6];
                            hexNodes[ringCount][i].lowerRowNeighbour.upperRowNeighbour = hexNodes[ringCount][i];
                        }
                        else
                        {
                            if (IsDecentNodeLeft(hexNodes[ringCount][i], hexNodes[ringCount - 1][i - 6], hexNodes[ringCount - 1][i - 5], origin))
                            {
                                hexNodes[ringCount][i].lowerRowNeighbour = hexNodes[ringCount - 1][i - 6];
                                hexNodes[ringCount][i].lowerRowNeighbour.upperRowNeighbour = hexNodes[ringCount][i];
                            }
                            else
                            {
                                hexNodes[ringCount][i].lowerRowNeighbour = hexNodes[ringCount - 1][i - 5];
                                hexNodes[ringCount][i].fallsClockwise = true;
                                hexNodes[ringCount][i].lowerRowNeighbour.upperRowNeighbour = hexNodes[ringCount][i];
                            }
                        }
                        i++;
                    }
                }
            }
            ringCount++;

        }
    }

    bool IsDecentNodeLeft(HexNode curr, HexNode left, HexNode right, Vector3 origin)
    {
        if (Vector3.Angle(curr.position + origin, left.position + origin) <= Vector3.Angle(curr.position + origin, right.position + origin))
            return true;
        else
            return false;
    }

    List<HexNode> GenerateHexRing(int ringIndex, Vector3 originPos)
    {
        List<HexNode> hexRing = new List<HexNode>();
        Vector3 lastPosition = originPos;

        if (ringIndex == 0)
            hexRing.Add(new HexNode(Vector3.zero));
        else
        {
            HexNode firstHex, prevHex, currHex = null;
            //top hex
            lastPosition = new Vector3(originPos.x, originPos.y + (2 * innerRadius * ringIndex), originPos.z);
            firstHex = new HexNode(lastPosition);
            hexRing.Add(firstHex);
            prevHex = firstHex;
            firstHex.fallsOnFlatEdge = true;

            //SE from top
            for (int i = 0; i < ringIndex; i++)
            {
                lastPosition = new Vector3(
                    lastPosition.x + (1.5f * outerRadius),
                    lastPosition.y - innerRadius,
                    lastPosition.z);
                currHex = new HexNode(lastPosition);
                currHex.antiClockwiseNeighbour = prevHex;
                prevHex.clockwiseNeighbour = currHex;
                hexRing.Add(currHex);
                prevHex = currHex;
                if(i==ringIndex-1)
                    currHex.fallsOnFlatEdge = true;

            }

            //S from SE
            for (int i = 0; i < ringIndex; i++)
            {
                lastPosition = new Vector3(
                    lastPosition.x,
                    lastPosition.y - (2 * innerRadius),
                    lastPosition.z);
                currHex = new HexNode(lastPosition);
                currHex.antiClockwiseNeighbour = prevHex;
                prevHex.clockwiseNeighbour = currHex;
                hexRing.Add(currHex);
                prevHex = currHex;
                if (i == ringIndex - 1)
                    currHex.fallsOnFlatEdge = true;
            }

            //SW from S
            for (int i = 0; i < ringIndex; i++)
            {
                lastPosition = new Vector3(
                    lastPosition.x - (1.5f * outerRadius),
                    lastPosition.y - innerRadius,
                    lastPosition.z);
                currHex = new HexNode(lastPosition);
                currHex.antiClockwiseNeighbour = prevHex;
                prevHex.clockwiseNeighbour = currHex;
                hexRing.Add(currHex);
                prevHex = currHex;
                if (i == ringIndex - 1)
                    currHex.fallsOnFlatEdge = true;
            }

            //NW from SW
            for (int i = 0; i < ringIndex; i++)
            {
                lastPosition = new Vector3(
                    lastPosition.x - (1.5f * outerRadius),
                    lastPosition.y + innerRadius,
                    lastPosition.z);
                currHex = new HexNode(lastPosition);
                currHex.antiClockwiseNeighbour = prevHex;
                prevHex.clockwiseNeighbour = currHex;
                hexRing.Add(currHex);
                prevHex = currHex;
                if (i == ringIndex - 1)
                    currHex.fallsOnFlatEdge = true;
            }

            //N from SW
            for (int i = 0; i < ringIndex; i++)
            {
                lastPosition = new Vector3(
                    lastPosition.x,
                    lastPosition.y + (2 * innerRadius),
                    lastPosition.z);
                currHex = new HexNode(lastPosition);
                currHex.antiClockwiseNeighbour = prevHex;
                prevHex.clockwiseNeighbour = currHex;
                hexRing.Add(currHex);
                prevHex = currHex;
                if (i == ringIndex - 1)
                    currHex.fallsOnFlatEdge = true;
            }

            //NE from N
            for (int i = 0; i < ringIndex - 1; i++)
            {
                lastPosition = new Vector3(
                    lastPosition.x + (1.5f * outerRadius),
                    lastPosition.y + innerRadius,
                    lastPosition.z);
                currHex = new HexNode(lastPosition);
                currHex.antiClockwiseNeighbour = prevHex;
                prevHex.clockwiseNeighbour = currHex;
                hexRing.Add(currHex);
                prevHex = currHex;
            }

            //last hex needs to be linked to the first
            firstHex.antiClockwiseNeighbour = currHex;
            currHex.clockwiseNeighbour = firstHex;
        }

        return hexRing;
    }

    //Public Functions
    public List<HexNode> GetHexRing(int ringIndex)
    {
        if (ringIndex < hexNodes.Count)
            return hexNodes[ringIndex];
        else
            return null;
    }

    public HexNode GetSpawnHex()
    {
        float degPerHex = 360f / outerHexNodes.Length;
        float offset = degPerHex / 2f;
        float angle = Vector3.SignedAngle(Vector3.up, CameraController.instance.transform.up, -CameraController.instance.transform.forward);
        if (angle < 0)
            angle += 360;
        angle += offset;

        return outerHexNodes[Mathf.Clamp(Mathf.FloorToInt(angle / degPerHex), 0, outerHexNodes.Length-1)];
    }

    //Gizmo Functions
    void OnDrawGizmos()
    {
        if (debugFallDirection == true || debugUpperRow == true || debugOccupied == true)
        {
            if (hexNodes != null)
            {
                foreach (List<HexNode> hexRing in hexNodes)
                {
                    if (debugFallDirection == true)
                    {
                        foreach (HexNode hex in hexRing)
                        {
                            if (hex.fallsOnFlatEdge)
                                Gizmos.color = Color.gray;
                            else if (hex.fallsClockwise)
                                Gizmos.color = Color.blue;
                            else
                                Gizmos.color = Color.green;

                            Gizmos.DrawWireMesh(hexGizmoMesh, hex.position, Quaternion.Euler(0, 0, 90f), Vector3.one);
                        }
                    }
                    if (debugUpperRow == true)
                    {
                        foreach (HexNode hex in hexRing)
                        {
                            if (hex.upperRowNeighbour == null)
                                Gizmos.color = Color.gray;
                            else
                                Gizmos.color = Color.green;

                            Gizmos.DrawWireMesh(hexGizmoMesh, hex.position, Quaternion.Euler(0, 0, 90f), Vector3.one);
                        }
                    }
                    if (debugOccupied == true)
                    {
                        foreach (HexNode hex in hexRing)
                        {
                            if (!hex.IsOccupied)
                                Gizmos.color = Color.gray;
                            else
                                Gizmos.color = Color.green;

                            Gizmos.DrawSphere(hex.position, 0.75f);
                        }
                    }
                }
            }
        }
    }
}
