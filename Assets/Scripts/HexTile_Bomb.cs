﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile_Bomb : HexTile
{
    [SerializeField] GameObject explosionEffect;
    [SerializeField] Transform effectSpawnPoint;

    private void Awake()
    {
        HexType = HexType.Bomb;
    }

    protected override void OnHexInitiated()
    {
        
    }

    protected override void OnHexDestroyed()
    {
        ObjectPool.Instance.ReuseObject(explosionEffect, effectSpawnPoint.position, effectSpawnPoint.rotation);
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
        MarkForRemoval(this);
        int i;
        for (i = 0; i < attachedHexTiles.Count; i++)
            if (attachedHexTiles[i].HexType != HexType.Center)
            {
                MarkForRemoval(attachedHexTiles[i]);
            }

        PlayerAchievements.hexesDestroyedWithBomb += i;
    }

    protected override void OnHexFinishAttaching()
    {
        MarkForActioning(this);
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
    }
}
