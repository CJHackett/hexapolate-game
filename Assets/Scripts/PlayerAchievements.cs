﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;

public class PlayerAchievements : MonoBehaviour
{
    public static int hexesDestroyed;
    public static int hexesDestroyedWithBomb;
    public static int sadTilesRemoved;

    const float level_complete_time_sec = 20f;
    const int max_level_required = 30;
    const int max_chain_required = 7;
    const int max_combo_required = 5;

    float gameStartTime;

    //singleton
    public static PlayerAchievements instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        gameStartTime = 0;
        hexesDestroyed = 0;
        hexesDestroyedWithBomb = 0;
    }

    public void BankScores()
    {
        //level progress
        AddLevelsCompleted(DifficultyController.currentLevel);
        ReportHighestLevel(DifficultyController.currentLevel);

        //hexes destroyed by bombs
        AddHexesDestroyed(hexesDestroyedWithBomb);
        hexesDestroyedWithBomb = 0;

        //hexes destroyed overall
        AddHexesDestroyed(hexesDestroyed);
        hexesDestroyed = 0;

        //sad tiles destroyed
        SadTilesDestroyed(sadTilesRemoved);
        sadTilesRemoved = 0;
    }

    public void MarkGameStart()
    {
        gameStartTime = Time.time;
    }
    public void MarkGameFinish()
    {
        ReportLevelCompletedTime(Time.time - gameStartTime);
    }

    //incremental

    public void AddLevelsCompleted(int levels)
    {

        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(
                GPGSIds.achievement_star_power,
                levels, (bool success) => { });

            PlayGamesPlatform.Instance.IncrementAchievement(
                GPGSIds.achievement_muchos_estrellas,
                levels, (bool success) => { });

            PlayGamesPlatform.Instance.IncrementAchievement(
                GPGSIds.achievement_happy_day,
                levels, (bool success) => { });
        }
    }

    public void AddHexesDestroyed_Bomb(int hexes)
    {

        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(
                GPGSIds.achievement_very_helpful,
                hexes, (bool success) => { });
        }
    }

    public void AddHexesDestroyed(int hexes)
    {

        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(
                GPGSIds.achievement_hex_master,
                hexes, (bool success) => { });
        }
    }

    public void SadTilesDestroyed(int tiles)
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.IncrementAchievement(
                GPGSIds.achievement_no_more_sadness,
                tiles, (bool success) => { });
        }
    }

    //instant completions

    public void ReportLevelCompletedByRainbow()
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ReportProgress(
                GPGSIds.achievement_rainbow_joy,
                100.0f, (bool success) => { });
        }
    }


    public void ReportHighestLevel(int level)
    {
        if (Social.localUser.authenticated && level >= max_level_required)
        {
            PlayGamesPlatform.Instance.ReportProgress(
                GPGSIds.achievement_unstoppable,
                100.0f, (bool success) => { });
        }
    }

    public void ReportLevelCompletedTime(float time)
    {
        if (Social.localUser.authenticated && time <= level_complete_time_sec)
        {
            PlayGamesPlatform.Instance.ReportProgress(
                GPGSIds.achievement_fast_mover,
                100.0f, (bool success) => { });
        }
    }

    public void ReportChainLength(int length)
    {
        if (Social.localUser.authenticated && length>=max_chain_required)
        {
            PlayGamesPlatform.Instance.ReportProgress(
                GPGSIds.achievement_strong_link,
                100.0f, (bool success) => { });
        }
    }

    public void ReportCombo(int combo)
    {
        if (Social.localUser.authenticated && combo >= max_combo_required)
        {
            PlayGamesPlatform.Instance.ReportProgress(
                GPGSIds.achievement_mega_combo,
                100.0f, (bool success) => { });
        }
    }
}
