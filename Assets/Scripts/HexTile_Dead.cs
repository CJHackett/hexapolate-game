﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile_Dead : HexTile
{
    const int destructionScore = 50;

    private void Awake()
    {
        HexType = HexType.Dead;
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
    }

    protected override void OnHexInitiated()
    {
    }

    protected override void OnHexDestroyed()
    {
        PlayerAchievements.sadTilesRemoved++;
        PlayerScore.instance.UpdateScore(destructionScore * currentMultiplier);
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
    }

    protected override void OnHexFinishAttaching()
    {
    }
}
