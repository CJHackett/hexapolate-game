﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    enum Orientation { Portrait, Landscape };

    //singleton
    public static CameraController instance = null;
    float angleToSlewTowards;
    [SerializeField] float rotationRateLandscape;
    [SerializeField] float rotationRatePortrait;
    float rotationRate;

    [Space]
    [SerializeField] float cameraSizePortrait;
    [SerializeField] float cameraSizeLandscape;
    [SerializeField] float cameraSizePortraitWholeScreen;
    [SerializeField] float cameraSizeLandscapeWholeScreen;
    [SerializeField] Camera attachedCamera;
    Orientation lastOrientation;
    [SerializeField] bool seeWholeMap;
    [SerializeField] bool dontFollowHex;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        angleToSlewTowards = transform.rotation.eulerAngles.z;
        UpdateCamera();
    }

    public void FollowPoint(Vector3 point)
    {
        angleToSlewTowards = Vector3.SignedAngle(Vector3.up, point, transform.forward);
    }

    private void Update()
    {
        if (Time.deltaTime > 0)
        {
            if (transform.rotation.eulerAngles.z != angleToSlewTowards && !dontFollowHex)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0.0f, 0.0f, angleToSlewTowards), (rotationRate/1000) * Time.time);
            }
        }

        if (OrientationChanged())
            UpdateCamera();
    }

    
    Orientation GetOrientation()
    {
        if (Screen.width > Screen.height)
        {
            return Orientation.Landscape;
        }
        else
        {
            return Orientation.Portrait;
        }
    }

    void UpdateCamera()
    {
        lastOrientation = GetOrientation();
        if(PlayerInput.Instance!=null)
            PlayerInput.Instance.UpdateButtonPoints();
        if (lastOrientation == Orientation.Portrait)
            SetForPortrait();
        else
            SetForLandscape();
    }

    bool OrientationChanged()
    {
        if (GetOrientation() != lastOrientation)
            return true;
        else
            return false;
    }

    void SetForPortrait()
    {
        if (seeWholeMap)
        {
            attachedCamera.orthographicSize = cameraSizePortraitWholeScreen;
            attachedCamera.transform.localPosition = new Vector3(attachedCamera.transform.localPosition.x, 0.0f, attachedCamera.transform.localPosition.z);
        }
        else
        {
            attachedCamera.orthographicSize = cameraSizePortrait;
            attachedCamera.transform.localPosition = new Vector3(attachedCamera.transform.localPosition.x, cameraSizePortrait, attachedCamera.transform.localPosition.z);
        }
        rotationRate = rotationRatePortrait;
    }

    void SetForLandscape()
    {

        if (seeWholeMap)
        {
            attachedCamera.orthographicSize = cameraSizeLandscapeWholeScreen;
            attachedCamera.transform.localPosition = new Vector3(attachedCamera.transform.localPosition.x, 0.0f, attachedCamera.transform.localPosition.z);
        }
        else
        {
            attachedCamera.orthographicSize = cameraSizeLandscape;
            attachedCamera.transform.localPosition = new Vector3(attachedCamera.transform.localPosition.x, cameraSizeLandscape, attachedCamera.transform.localPosition.z);
        }
        rotationRate = rotationRateLandscape;
    }
}
