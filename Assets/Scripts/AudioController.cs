﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioController : MonoBehaviour {

    public const float MUTE = -80f;

    public AudioMixer audioMixer;
    public Sprite mutedImage;
    public Sprite unMutedImage;
    const float unMuteMasterVolume = -10f;
    const float muteMasterVolume = -80f;
    float masterVolume;
    bool musicStarted;

    public const float musicVolumeMaxSetting = 0f;
    public const float musicVolumeMinSetting = -40f;
    public const float sfxVolumeMaxSetting = 0f;
    public const float sfxVolumeMinSetting = -20f;

    //Music
    [Space]
    public AudioSource musicSource;
    //SFX 
    [Space]
    public AudioSource sfxSource;
    public AudioClip hexMove;
    public AudioClip hexPlace;
    public AudioClip hexDestroy;
    public AudioClip gameStart;
    public AudioClip gameOver;
    //UI Sounds
    [Space]
    public AudioSource uiSoundSource;
    public AudioClip uiClick;

    Queue<AudioClip> sfxClipsToPlay = new Queue<AudioClip>();

    //singleton
    public static AudioController instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        SetMusicVolume(GameSettings.instance.MusicVolume);
        SetSfxVolume(GameSettings.instance.SfxVolume);

        if (!GameSettings.instance.Muted)
        {
            SetMasterVolume(unMuteMasterVolume);
            musicSource.mute = false;
            musicStarted = true;
            musicSource.Play();
        }
        else
        {
            SetMasterVolume(muteMasterVolume);
            musicSource.mute = true;
            musicStarted = false;
        }
    }
    private void Update()
    {
        while (sfxClipsToPlay.Count > 0)
        {
            sfxSource.PlayOneShot(sfxClipsToPlay.Dequeue());
        }
    }

    public void Mute(Image imageToSwitch)
    {
        SetMasterVolume(muteMasterVolume);
        GameSettings.instance.Muted = true;

        musicSource.mute = true;
        if(musicStarted)
            musicSource.Pause();

        imageToSwitch.sprite = mutedImage;
    }

    public void UnMute(Image imageToSwitch)
    {
        SetMasterVolume(unMuteMasterVolume);
        GameSettings.instance.Muted = false;
        
        musicSource.mute = false;
        if (!musicStarted)
        {
            musicStarted = true;
            musicSource.Play();
        }
        else
            musicSource.UnPause();

        imageToSwitch.sprite = unMutedImage;
    }

    public void RefreshMuteButton(Image imageToSwitch)
    {
        if (!IsMuted())
            imageToSwitch.sprite = unMutedImage;
        else
            imageToSwitch.sprite = mutedImage;
    }

    public bool IsMuted()
    {
        if (masterVolume == unMuteMasterVolume)
            return false;
        else
            return true;
    }

    public void PlayHexMove()
    {
        if (!sfxClipsToPlay.Contains(hexMove))
            sfxClipsToPlay.Enqueue(hexMove);
    }
    public void PlayHexPlace()
    {
        if (!sfxClipsToPlay.Contains(hexPlace))
            sfxClipsToPlay.Enqueue(hexPlace);
    }
    public void PlayHexDestroy()
    {
        if (!sfxClipsToPlay.Contains(hexDestroy))
            sfxClipsToPlay.Enqueue(hexDestroy);
    }
    public void PlayGameStart()
    {
        sfxSource.PlayOneShot(gameStart);
    }
    public void PlayGameOver()
    {
        sfxSource.PlayOneShot(gameOver);
    }
    public void PlayClickUI()
    {
        uiSoundSource.PlayOneShot(uiClick);
    }

    public void PauseSfx()
    {
        sfxSource.Pause();
    }
    public void UnPauseSfx()
    {
        sfxSource.UnPause();
    }

    void SetMasterVolume(float value)
    {
        masterVolume = value;
        audioMixer.SetFloat("masterVolume", value);
    }
    public void SetMusicVolume(float value)
    {
        if (value == MUTE)
        {
            audioMixer.SetFloat("musicVolume", MUTE);
        }
        else
        {
            if (value > musicVolumeMaxSetting)
                audioMixer.SetFloat("musicVolume", musicVolumeMaxSetting);
            else if (value < musicVolumeMinSetting)
                audioMixer.SetFloat("musicVolume", musicVolumeMinSetting);
            else
                audioMixer.SetFloat("musicVolume", value);
        }
    }
    public void SetSfxVolume(float value)
    {
        if (value == MUTE)
        {
            audioMixer.SetFloat("sfxVolume", MUTE);
        }
        else
        {
            if (value > sfxVolumeMaxSetting)
                audioMixer.SetFloat("sfxVolume", sfxVolumeMaxSetting);
            else if (value < sfxVolumeMinSetting)
                audioMixer.SetFloat("sfxVolume", sfxVolumeMinSetting);
            else
                audioMixer.SetFloat("sfxVolume", value);
        }
    }
}
