﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile_Random : HexTile
{
    [SerializeField] GameObject touchEffect;
    [SerializeField] Transform effectSpawnPoint;
    [SerializeField] GameObject[] nonPlainOptionsToSpawn;
    GameObject[] spawnOptions;
    int plainHexIndexStart = 3;

    void Awake()
    {
        HexType = HexType.Random;
    }

    private void Start()
    {
        //get the plain hex tiles in the randomised order in the hex generator
        List<GameObject> options = new List<GameObject>();
        for(int i = 0; i < nonPlainOptionsToSpawn.Length; i++)
        {
            options.Add(nonPlainOptionsToSpawn[i]);
        }
        for (int i = 0; i < HexGenerator.instance.PlainHex.Length; i++)
        {
            options.Add(HexGenerator.instance.PlainHex[i]);
        }
        spawnOptions = options.ToArray();
    }

    protected override void OnHexInitiated()
    {


    }

    protected override void OnHexFinishAttaching()
    {
        //can only spawn options that are available 
        int upperRange = plainHexIndexStart + DifficultyController.currColours;
        GameObject objectToSpawn = spawnOptions[Random.Range(0, upperRange)];

        if (objectToSpawn != null)
        {
            ObjectPool.Instance.ReuseObject(touchEffect, new Vector3(CurrNode.position.x, CurrNode.position.y, effectSpawnPoint.position.z), effectSpawnPoint.rotation);

            ReplaceHexTile(objectToSpawn);
        }
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
    }

    protected override void OnHexDestroyed()
    {
    }
}
