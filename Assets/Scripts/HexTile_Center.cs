﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile_Center : HexTile
{
    [SerializeField] GameObject onTouchEffect;

    private void Awake()
    {
        HexType = HexType.Center;
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {
        //stop dropping hexes
        if (otherHex.HexType != HexType.Dead && otherHex.HexType != HexType.Barrier)
        {
            PlayerAchievements.instance.MarkGameFinish();

            onTouchEffect.gameObject.SetActive(true);

            if (otherHex.HexType == HexType.Rainbow)
                PlayerAchievements.instance.ReportLevelCompletedByRainbow();

            HexGenerator.StopGeneratingHexes();
            hexTilesToBeActioned.Clear();
            hexTilesToBeChecked.Clear();
            hexTilesToBeRemoved.Clear();

            StartCoroutine(EndGame());
        }
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
    }

    protected override void OnHexInitiated()
    {
    }

    protected override void OnHexDestroyed()
    {
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
    }

    protected override void OnHexFinishAttaching()
    {
    }

    IEnumerator EndGame()
    {
        HexTile[] tiles = tileRegister.ToArray();
        for (int i = 0; i < tiles.Length; i++)
            if (tiles[i].HexType != HexType.Center)
                MarkForRemoval(tiles[i]);

        while (hexesAreActive)
        {
            yield return null;
        }

        DifficultyController.IncreaseDifficulty();
        HexGenerator.instance.ResetBarrierTiles();
    }
}
