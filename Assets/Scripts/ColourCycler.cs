﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColourCycler : MonoBehaviour
{
    public float changeColourTime = 2.0f;

    private float timer = 0.0f;

    [SerializeField] Color[] colorsToCycle;
    int currentIndex = 0;
    private int nextIndex;
    [SerializeField] Renderer hexRenderer;
    [SerializeField] Image image;
    bool sourceIsImage;
    Material[] materials;

    void Awake()
    {
        if (hexRenderer != null)
        {
            materials = hexRenderer.materials;
            sourceIsImage = false;
        }
        else
        {
            sourceIsImage = true;
        }
        nextIndex = (currentIndex + 1) % colorsToCycle.Length;
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer > changeColourTime)
        {
            currentIndex = (currentIndex + 1) % colorsToCycle.Length;
            nextIndex = (currentIndex + 1) % colorsToCycle.Length;
            timer = 0.0f;

        }
        if (sourceIsImage)
        {
            image.color = Color.Lerp(colorsToCycle[currentIndex], colorsToCycle[nextIndex], timer / changeColourTime);
        }
        else
        {
            for (int i = 0; i < materials.Length; i++)
                materials[i].color = Color.Lerp(colorsToCycle[currentIndex], colorsToCycle[nextIndex], timer / changeColourTime);
        }
    }
}
