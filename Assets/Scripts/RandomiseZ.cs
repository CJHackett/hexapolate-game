﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomiseZ : MonoBehaviour
{
    void Start()
    {
        transform.localRotation = Quaternion.Euler(transform.localRotation.x, transform.localRotation.y, Random.Range(0, 360));
    }
}
