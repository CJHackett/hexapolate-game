﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public enum MenuPanels { Splash, Main, About, QuitPanel, HowToPanel, Options, Loading, Notification};

public class MainMenuNavigation : MonoBehaviour {

    public MenuObject[] menus;
    MenuObject openMenu;
    [SerializeField] Text notificationText;
    [Space]
    public AdController ads;
    [Space]
    AudioController audioController;
    public Image muteButtonImage;
    public float panelFadeInAlphaRate;

    float backButtonDelay = 0.2f;
    float timeSinceLastBackPress;
    bool currentlyFading;
    [Range(0, 100)] public int startLevelEasy;
    [Range(0, 100)] public int startLevelMed;
    [Range(0, 100)] public int startLevelHard;

    private void Awake()
    {
        currentlyFading = false;
        audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
    }

    void Start(){
        timeSinceLastBackPress = Time.time;

        for(int i = 0; i < menus.Length; i++)
        {
            if (menus[i].menuName == MenuPanels.Splash)
            {
                menus[i].panel.SetActive(true);
                openMenu = menus[i];
            }
            else
                menus[i].panel.SetActive(false);
        }
        
    }

    private void Update()
    {
        audioController.RefreshMuteButton(muteButtonImage);
        if (Time.time > timeSinceLastBackPress + backButtonDelay)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                GoBack();
                timeSinceLastBackPress = Time.time;
            }
        }
    }

    public void MuteButtonPressed()
    {
        if (audioController.IsMuted())
        {
            AudioController.instance.PlayClickUI();
            audioController.UnMute(muteButtonImage);
        }
        else
            audioController.Mute(muteButtonImage);
    }

    public void GoBack()
    {
        OpenPanel(openMenu.previousMenu);
    }

	public void OpenPanel (int panel){
        //only called when the game is opened first time once pressing from the splash panel
        if (panel == (int)MenuPanels.Main && GameSettings.instance.FirstTimePlaying)
        {
            openMenu.panel.SetActive(false);
            openMenu = menus[(int)MenuPanels.Main];
            OpenNotificationPanel("HI, WELCOME TO HEXAPOLATE AND THANK YOU FOR DOWNLOADING.\nTO AID YOUR FIRST TIME PLAYING, PLEASE HAVE A LOOK AT THE 'HOW TO PLAY' MENU.\nDIFFICULTY HAS BEEN SET TO EASY, BUT HARDER DIFFICULTIES CAN BE FOUND IN THE SETTINGS WHEN UNLOCKED.\nHAPPY GAMING!");
            GameSettings.instance.FirstTimePlaying = false;
            return;
        }

        AudioController.instance.PlayClickUI();
        if (currentlyFading == true)
            StopAllCoroutines();

        openMenu.canvasGroup.interactable = false;
        menus[panel].canvasGroup.interactable = false;

        StartCoroutine(FadePanel(panel));
	}
    IEnumerator FadePanel(int panel)
    {
        currentlyFading = true;
        //fade out the panel
        float alpha = openMenu.canvasGroup.alpha;
        while (alpha > 0f)
        {
            yield return null;
            alpha -= Time.deltaTime * panelFadeInAlphaRate;
            if (alpha < 0)
                alpha = 0;
            openMenu.canvasGroup.alpha = alpha;
        }
        openMenu.panel.SetActive(false);
        openMenu.canvasGroup.alpha = 1;

        openMenu = menus[panel];
        openMenu.canvasGroup.alpha = 0;
        menus[panel].panel.SetActive(true);
        alpha = 0f;
        while (alpha < 1f)
        {
            yield return null;
            alpha += Time.deltaTime * panelFadeInAlphaRate;
            if (alpha > 1)
                alpha = 1;
            openMenu.canvasGroup.alpha = alpha;
        }
        openMenu.canvasGroup.interactable = true;
        currentlyFading = false;
    }

    public void OpenNotificationPanel(string notification)
    {
        menus[(int)MenuPanels.Notification].previousMenu = (int)openMenu.menuName;
        notificationText.text = notification;
        OpenPanel((int)MenuPanels.Notification);
    }

    public void GameStart()
    {
        if (GameSettings.instance.DebugEnabled)
            ads.OpenPreGameAd(GameSettings.instance.StartLevel);
        else
        {
            switch (GameSettings.instance.Difficulty)
            {
                case GameDifficulty.Easy:
                    ads.OpenPreGameAd(startLevelEasy);
                    break;
                case GameDifficulty.Medium:
                    ads.OpenPreGameAd(startLevelMed);
                    break;
                case GameDifficulty.Hard:
                    ads.OpenPreGameAd(startLevelHard);
                    break;
            }
        }
    }

    public void OpenPrivacyPolicy()
    {
        Application.OpenURL("https://sites.google.com/view/hexapolateprivacypolicy/home");
    }

    public void QuitGame(){
        AudioController.instance.PlayClickUI();
        Application.Quit ();
	}

    public void StartGame(int startLevel)
    {
        GameSettings.instance.StartLevel = startLevel;
        AudioController.instance.PlayClickUI();
        OpenPanel((int)MenuPanels.Loading);
        StartCoroutine(WaitToStartGame());
    }
    IEnumerator WaitToStartGame()
    {
        while (currentlyFading)
        {
            yield return null;
        }
        SceneManager.LoadScene("Main");
    }

    [System.Serializable]
    public struct MenuObject
    {
        public MenuPanels menuName;
        public GameObject panel;
        public int previousMenu;
        public CanvasGroup canvasGroup;
    }

}


