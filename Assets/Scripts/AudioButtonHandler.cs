﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioButtonHandler : MonoBehaviour
{
    const int sliderMax = 10;
    const int sliderMin = 1;

    [SerializeField] Text musicVolumeText;
    [SerializeField] Text effectsVolumeText;

    int musicVolumeValue;
    int effectsVolumeValue;

    private void Start()
    {
        musicVolumeValue = ConvertFromDecibels(GameSettings.instance.MusicVolume, true);
        effectsVolumeValue = ConvertFromDecibels(GameSettings.instance.SfxVolume, false);
        UpdateMusicVolumeText();
        UpdateSfxVolumeText();
    }

    public void MusicVolumeIncrease()
    {
        if (musicVolumeValue < sliderMax)
        {
            musicVolumeValue++;
            AudioController.instance.SetMusicVolume(ConvertToDecibels(musicVolumeValue, true));
            GameSettings.instance.MusicVolume = (ConvertToDecibels(musicVolumeValue, true));
            AudioController.instance.PlayClickUI();
            UpdateMusicVolumeText();
        }
    }
    public void MusicVolumeDecrease()
    {
        if (musicVolumeValue >= sliderMin)
        {
            musicVolumeValue--;
            AudioController.instance.SetMusicVolume(ConvertToDecibels(musicVolumeValue, true));
            GameSettings.instance.MusicVolume = (ConvertToDecibels(musicVolumeValue, true));
            AudioController.instance.PlayClickUI();
            UpdateMusicVolumeText();
        }
    }


    public void EffectsVolumeIncrease()
    {
        if (effectsVolumeValue < sliderMax)
        {
            effectsVolumeValue++;

            AudioController.instance.SetSfxVolume(ConvertToDecibels(effectsVolumeValue, false));
            GameSettings.instance.SfxVolume = (ConvertToDecibels(effectsVolumeValue, false));
            AudioController.instance.PlayClickUI();
            UpdateSfxVolumeText();
        }
    }
    public void EffectsVolumeDecrease()
    {
        if (effectsVolumeValue >= sliderMin)
        {
            effectsVolumeValue--;

            AudioController.instance.SetSfxVolume(ConvertToDecibels(effectsVolumeValue, false));
            GameSettings.instance.SfxVolume = (ConvertToDecibels(effectsVolumeValue, false));
            AudioController.instance.PlayClickUI();
            UpdateSfxVolumeText();
        }
    }

    float ConvertToDecibels(int value, bool isMusic)
    {
        if (value == 0)
            return AudioController.MUTE;

        float currFraction = Mathf.InverseLerp(sliderMin, sliderMax, value);
        if (isMusic == true)
        {
            return Mathf.Lerp(AudioController.musicVolumeMinSetting, AudioController.musicVolumeMaxSetting, currFraction);
        }else
        {
            return Mathf.Lerp(AudioController.sfxVolumeMinSetting, AudioController.sfxVolumeMaxSetting, currFraction);
        }
        
    }
    int ConvertFromDecibels(float value, bool isMusic)
    {
        if (value == AudioController.MUTE)
            return 0;

        float volumeFraction;
        if (isMusic == true)
        {
            volumeFraction = Mathf.InverseLerp(AudioController.musicVolumeMinSetting, AudioController.musicVolumeMaxSetting, value);
        }
        else
        {
            volumeFraction = Mathf.InverseLerp(AudioController.sfxVolumeMinSetting, AudioController.sfxVolumeMaxSetting, value);
        }

        return (int)(Mathf.Lerp(sliderMin, sliderMax, volumeFraction));
    }

    void UpdateMusicVolumeText()
    {
        //music volume is provided in raw float form, need to convert to value between 1 and 10
        musicVolumeText.text = musicVolumeValue.ToString();

    }
    void UpdateSfxVolumeText()
    {
        //music volume is provided in raw float form, need to convert to value between 1 and 10
        effectsVolumeText.text = effectsVolumeValue.ToString();
    }

}
