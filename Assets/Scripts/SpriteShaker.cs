﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteShaker : MonoBehaviour
{
    [SerializeField] GameObject objectToShake;
    [SerializeField] float shakeSpeed = 0.1f;
    [SerializeField] float totalShakeDuration = 0.5f; //must be greater than the decreasePoint
    [SerializeField] float decreasePoint = 0.2f;
    bool shaking = false;

    Transform objTransform;
    Vector3 defaultPos;

    private void Awake()
    {
        objTransform = objectToShake.transform;
        defaultPos = objTransform.localPosition;
    }

    private void OnDisable()
    {
        StopShaking();
    }

    public void StopShaking()
    {
        if (shaking)
        {
            StopAllCoroutines();
            shaking = false;
        }
        objTransform.localPosition = defaultPos;
    }

    IEnumerator shakeGameObjectCOR(bool objectIs2D = false)
    {
        float counter = 0f;

        //Do the actual shaking
        while (counter < totalShakeDuration)
        {
            counter += Time.deltaTime;
            float decreaseSpeed = shakeSpeed;

            //Shake GameObject
            if (objectIs2D)
            {
                //Don't Translate the Z Axis if 2D Object
                Vector3 tempPos = defaultPos + UnityEngine.Random.insideUnitSphere * decreaseSpeed;
                tempPos.z = defaultPos.z;
                objTransform.localPosition = tempPos;
            }
            else
            {
                objTransform.localPosition = defaultPos + UnityEngine.Random.insideUnitSphere * decreaseSpeed;
            }
            yield return null;


            //Check if we have reached the decreasePoint then start decreasing  decreaseSpeed value
            if (counter >= decreasePoint)
            {

                //Reset counter to 0 
                counter = 0f;
                while (counter <= decreasePoint)
                {
                    counter += Time.deltaTime;
                    decreaseSpeed = Mathf.Lerp(shakeSpeed, 0, counter / decreasePoint);

                    //Shake GameObject
                    if (objectIs2D)
                    {
                        //Don't Translate the Z Axis if 2D Object
                        Vector3 tempPos = defaultPos + UnityEngine.Random.insideUnitSphere * decreaseSpeed;
                        tempPos.z = defaultPos.z;
                        objTransform.localPosition = tempPos;
                    }
                    else
                    {
                        objTransform.localPosition = defaultPos + UnityEngine.Random.insideUnitSphere * decreaseSpeed;
                    }
                    yield return null;
                }

                //Break from the outer loop
                break;
            }
        }

        objTransform.localPosition = defaultPos; //Reset to original postion
        shaking = false; //So that we can call this function next time
    }


    public void ShakeGameObject(bool objectIs2D = true)
    {
        if (shaking)
        {
            return;
        }
        shaking = true;
        StartCoroutine(shakeGameObjectCOR(objectIs2D));
    }
}
