﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighlightButtonHandler : MonoBehaviour
{
    [SerializeField] Image buttonImage;
    [SerializeField] Sprite enabledImage;
    [SerializeField] Sprite disabledImage;

    private void Start()
    {
        UpdateButtonState();
    }

    public void ButtonPressed()
    {
        PathHighlighter.instance.ToggleHighlighting();
        UpdateButtonState();
    }

    void UpdateButtonState()
    {
        if (GameSettings.instance.ShowPathHighlight)
            buttonImage.enabled = true;
        //buttonImage.sprite = enabledImage;
        else
            //buttonImage.sprite = disabledImage;
            buttonImage.enabled = false;
    }
}
