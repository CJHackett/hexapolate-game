using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class GoogleSignIn : MonoBehaviour {
    
	public GameObject loadingPanel;
    public Text loadingText;
	public Button achievementButton;
	public Button leaderboardButton;
	public Button signInButton;
    public Button loadingPanelButton;

    bool authenticating = false;

	// Use this for initialization
	void Start () {
        loadingPanel.SetActive(false);
        loadingPanelButton.interactable = false;
        PlayGamesPlatform.Activate();
		if (PlayerPrefs.GetInt ("LastSignInStatus",0) > 0)
			AuthenticateGoogleUser ();
	}

    //checks status of authentication
    private void Update()
    {
        if (!authenticating)
        {
            if (Social.localUser.authenticated)
            {
                signInButton.interactable = false;
                achievementButton.interactable = true;
                leaderboardButton.interactable = true;
            }
            else
            {
                signInButton.interactable = true;
                achievementButton.interactable = false;
                leaderboardButton.interactable = false;
            }
        }
    }

    public void SignInButtonPressed(){
        if (!Social.localUser.authenticated)
        {
            loadingPanel.SetActive(true);
            loadingText.text = "SIGNING IN...";
            loadingPanelButton.interactable = false;
            AuthenticateGoogleUser();
        }
	}

	public void AuthenticateGoogleUser(){
        //lock buttons
        authenticating = true;
        achievementButton.interactable = false;
        signInButton.interactable = false;
        leaderboardButton.interactable = false;

        //attemt authenication
        Social.localUser.Authenticate ((bool success) => {
			if (success){
				PlayerPrefs.SetInt ("LastSignInStatus", 1);
                loadingPanel.SetActive(false);
                loadingPanelButton.interactable = false;

                authenticating = false;
            }
			else{
                loadingText.text = "SIGN IN FAILED";
                PlayerPrefs.SetInt ("LastSignInStatus", 0);
                loadingPanelButton.interactable = true;
            }
		});

        
	}

	public void FailedPanelReturn(){
		loadingPanel.SetActive (false);
        authenticating = false;
    }

    public void OpenLeaderboard()
    {
        Social.ShowLeaderboardUI();
    }

    public void OpenAchievements()
    {
        Social.ShowAchievementsUI();
    }
}
