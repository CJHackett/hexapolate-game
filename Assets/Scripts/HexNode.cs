﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexNode
{
    public Vector3 position;
    public HexTile occupyingHexTile;
    public HexNode clockwiseNeighbour;
    public HexNode antiClockwiseNeighbour;
    public HexNode lowerRowNeighbour;
    public HexNode upperRowNeighbour;
    public bool fallsOnFlatEdge;
    public bool fallsClockwise;

    public HexNode(Vector3 _position)
    {
        position = _position;
        occupyingHexTile = null;
        clockwiseNeighbour = null;
        antiClockwiseNeighbour = null;
        lowerRowNeighbour = null;
        upperRowNeighbour = null;
        fallsOnFlatEdge = false;
        fallsClockwise = false;
    }

    public bool IsCenter
    {
        get
        {
            if (lowerRowNeighbour == null)
                return true;
            else
                return false;
        }
    }
    public bool IsOccupied
    {
        get
        {
            if (occupyingHexTile == null)
                return false;
            else
                return true;
        }
    }
}

