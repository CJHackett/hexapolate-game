﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionUpdater : MonoBehaviour
{
    [SerializeField] Text versionText;

    void Start()
    {
        versionText.text = "Version : " + Application.version.ToString();
    }

}
