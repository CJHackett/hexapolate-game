﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HexType { Center, Barrier, Plain, Dead, Rainbow, Bomb, Random, Spin, Shuffle }

public abstract class HexTile : PooledObject
{
    //model of the hex to enable and disable when not spawned
    [SerializeField] protected GameObject hexModel;
    [SerializeField] protected SpriteShaker shakerScript;
    Vector3 targetPosition;
    const float hexMoveSlerpSpeed = 30f;

    //hex is active if falling
    [HideInInspector] public bool hexIsControllable;
    public static HexTile activeHex = null;

    //global variable to indicated if any hex is moving
    public static bool hexesAreActive;
    static bool actionCoroutineRunning = false;

    //hex type is set in the children of this class
    public HexType HexType { get; protected set; }

    //delay before letting hexes drop uncontrolled
    float activeHexesWaitDelay_sec = 0.1f;

    //holds the information of the current node this hex occupies
    [SerializeField] HexNode currNode;
    public HexNode CurrNode { get => currNode; }

    //holds the hexTiles currently touching this hex
    [SerializeField] protected List<HexTile> attachedHexTiles = new List<HexTile>();

    //register of all tiles
    public static List<HexTile> tileRegister = new List<HexTile>();

    //variable tracks current hex destroy multipliers
    public static int currentMultiplier = 1;

    //class members
    public bool HexInitiated { get; private set; }
    public bool CanDamageBarrier { get; set; }

    //abstract methods
    protected abstract void OnHexInitiated();
    protected abstract void OnHexFinishAttaching();
    protected abstract void OnHexEndOfTurnActionCallback();
    protected abstract void OnHexNeighbourConnected(HexTile otherHex);
    protected abstract void OnHexNeighbourRemoved(HexTile otherHex);
    protected abstract void OnHexDestroyed();


    //----------------------------------------------
    //---------------Hex Management-----------------
    //----------------------------------------------

    //called when instantiated
    public override void OnObjectReuse()
    {
        hexModel.SetActive(true);
        hexModel.transform.rotation = Quaternion.identity;
        HexInitiated = false;
    }

    //updates the position of the hex to move to the target position
    protected virtual void Update()
    {
        if (targetPosition != transform.position && HexInitiated)
            transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * hexMoveSlerpSpeed);
    }

    //called when hex is placed in game
    public void InitHexTile(HexNode node, bool startFalling)
    {
        //resets variables
        attachedHexTiles.Clear();
        tileRegister.Add(this);
        HexInitiated = true;
        CanDamageBarrier = false;
        hexIsControllable = false;
        hexesAreActive = true;
        RemoveMarks();

        //attaches to node
        AttachToNode(node, true);

        //look towards center of map
        if (!currNode.IsCenter)
            LookAtCenterTile();

        //expand the tile for visual effect
        StartCoroutine(ExpandModel());

        OnHexInitiated();

        //starts falling if required, otherwise added to checking list
        if (startFalling)
        {
            StartHexControlledDecent();
        }
        else
        {
            MarkForChecking(this);
            AudioController.instance.PlayHexPlace();
        }
    }

    void AttachToNode(HexNode node, bool snapToLocation)
    {
        currNode = node;
        node.occupyingHexTile = this;
        if(snapToLocation)
            transform.position = node.position;
        targetPosition = node.position;
    }
    void DetachFromNode()
    {
        currNode.occupyingHexTile = null;
        currNode = null;
    }

    //called when hex is removed from its node and starts its shrink animation
    public void DestroyHexTile()
    {
        DetachFromNode();
        tileRegister.Remove(this);
        HexInitiated = true;

        DetachFromNeighbouringHexTiles();

        OnHexDestroyed();
        AudioController.instance.PlayHexDestroy();
        StartCoroutine(ShrinkModel());

        if (shakerScript != null)
            shakerScript.StopShaking();
    }

    //Performs a mix of destroying the old tile and removing neighbour connections without starting a chain of checks.
    //The new tile reperforms the checks when connected.
    public void ReplaceHexTile(GameObject newTile)
    {
        HexNode oldNode = CurrNode;
        DestroyHexTile();

        HexTile spawnedTile = HexGenerator.instance.InstantiateHex(newTile, oldNode.position);
        spawnedTile.InitHexTile(oldNode, false);
    }

    public bool IsTileDestroyed
    {
        get
        {
            if (currNode == null)
                return true;
            else
                return false;
        }
    }




    //----------------------------------------------
    //---------------Hex Actioning------------------
    //----------------------------------------------

    bool ActionCoroutineRequired
    {
        get
        {
            return (HexTilesRequireAction && actionCoroutineRunning == false);
        }
    }
    bool HexTilesRequireAction
    {
        get
        {
            return (hexTilesToBeActioned.Count > 0 || hexTilesToBeChecked.Count > 0 || hexTilesToBeRemoved.Count > 0);
        }
    }

    //finished up all actions to be carried out as a result of the tiles moving. Carried out only on the center tile as it will always be present
    void LateUpdate()
    {
        if (!IsTileDestroyed)
            if (currNode.IsCenter)
                if (ActionCoroutineRequired == true)
                    StartCoroutine(ActionCoroutine());

    }

    IEnumerator ActionCoroutine()
    {
        actionCoroutineRunning = true;
        hexesAreActive = true;
        while (HexTilesRequireAction)
        {
            //perform checks on hexes
            while (hexTilesToBeChecked.Count > 0)
            {

                HexTile hex = GetNextCheckingHex();
                if (!hex.IsTileDestroyed)
                {
                    //check to see if hex should drop
                    //Debug.Log("Checking hex can drop" + hex);
                    if (hex.CanDecend)
                    {
                        //Debug.Log("Hex can drop" + hex);
                        hex.DetachFromNeighbouringHexTiles();

                        while (hex.CanDecend)
                        {
                            hex.MoveToLowerNode(false);
                            yield return new WaitForSeconds(activeHexesWaitDelay_sec);
                        }
                    }

                    //Debug.Log("Hex tile checked" + hex);
                    //attach to neighbours if not already
                    hex.AttachToNeighbouringHexTiles();

                    //callback for when hexes are finished attaching
                    AudioController.instance.PlayHexPlace();
                    hex.OnHexFinishAttaching();
                }
            }




            //perform all actions requested by hex tiles
            if (hexTilesToBeActioned.Count > 0)
            {
                currentMultiplier++;
                //Debug.Log("current multiplier: " + currentMultiplier);
            }
            while (hexTilesToBeActioned.Count > 0)
            {

                HexTile hex = GetNextActioningHex();
                if (!hex.IsTileDestroyed)
                {
                    //Debug.Log("Hex tile actioned");
                    hex.OnHexEndOfTurnActionCallback();
                }
            }



            //perform all removal of hexes
            while (hexTilesToBeRemoved.Count > 0)
            {

                HexTile hex = GetNextRemovalHex();
                if (!hex.IsTileDestroyed)
                {
                    hex.DestroyHexTile();
                    PlayerAchievements.hexesDestroyed++;
                    yield return new WaitForSeconds(activeHexesWaitDelay_sec);
                }
            }
        }
        actionCoroutineRunning = false;
        hexesAreActive = false;
        PlayerAchievements.instance.ReportCombo(currentMultiplier - 1);
        currentMultiplier = 1;
    }

    //informs this tile of its neighbours, and alerts the new neighbours of this tile
    void AttachToNeighbouringHexTiles()
    {
        //Debug.Log("Connecting to neighbours"+this);

        //return if center as nothing to attach to
        if (currNode.IsCenter)
            return;

        //clockwise neighbour
        if (currNode.clockwiseNeighbour.IsOccupied)
        {
            //Debug.Log("Tile connected to clockwise neighbour");
            //AddNeighbourHexTile(currNode.clockwiseNeighbour.occupyingHexTile);
            currNode.clockwiseNeighbour.occupyingHexTile.ConnectHex(this);
        }

        //anticlockwise neighbour
        if (currNode.antiClockwiseNeighbour.IsOccupied)
        {
            //Debug.Log("Tile connected to anticlockwise neighbour");
            //AddNeighbourHexTile(currNode.antiClockwiseNeighbour.occupyingHexTile);
            currNode.antiClockwiseNeighbour.occupyingHexTile.ConnectHex(this);
        }

        //lower neighbour
        if (currNode.lowerRowNeighbour.IsOccupied)
        {
            //Debug.Log("Tile connected to lower neighbour");
            //AddNeighbourHexTile(currNode.lowerRowNeighbour.occupyingHexTile);
            currNode.lowerRowNeighbour.occupyingHexTile.ConnectHex(this);
        }
        //special case for falling on point, needs to check also clockwise or anticlockwise also
        if (!currNode.fallsOnFlatEdge)
        {
            if (currNode.fallsClockwise)
            {
                if (currNode.lowerRowNeighbour.antiClockwiseNeighbour.IsOccupied)
                {
                    //Debug.Log("Tile connected to lower anticlockwise neighbour");
                    //AddNeighbourHexTile(currNode.lowerRowNeighbour.antiClockwiseNeighbour.occupyingHexTile);
                    currNode.lowerRowNeighbour.antiClockwiseNeighbour.occupyingHexTile.ConnectHex(this);
                }
            }
            else
            {
                if (currNode.lowerRowNeighbour.clockwiseNeighbour.IsOccupied)
                {
                    //Debug.Log("Tile connected to lower clockwise neighbour");
                    //AddNeighbourHexTile(currNode.lowerRowNeighbour.clockwiseNeighbour.occupyingHexTile);
                    currNode.lowerRowNeighbour.clockwiseNeighbour.occupyingHexTile.ConnectHex(this);
                }
            }
        }

        //upper neighbour if not on top row
        if (currNode.upperRowNeighbour != null)
        {
            if (currNode.upperRowNeighbour.IsOccupied)
            {
                //Debug.Log("Tile connected to upper neighbour");
                //AddNeighbourHexTile(currNode.upperRowNeighbour.occupyingHexTile);
                currNode.upperRowNeighbour.occupyingHexTile.ConnectHex(this);
            }
            //special case for falling on point, needs to check also clockwise or anticlockwise also. If flat, will check upperclockwise and upperclockwise
            if (!currNode.upperRowNeighbour.fallsOnFlatEdge)
            {
                if (currNode.upperRowNeighbour.fallsClockwise)
                {
                    //Debug.Log("Tile connected to upper clockwise neighbour");
                    if (currNode.upperRowNeighbour.clockwiseNeighbour.IsOccupied)
                    {
                        //AddNeighbourHexTile(currNode.upperRowNeighbour.clockwiseNeighbour.occupyingHexTile);
                        currNode.upperRowNeighbour.clockwiseNeighbour.occupyingHexTile.ConnectHex(this);
                    }
                }
                else
                {
                    if (currNode.upperRowNeighbour.antiClockwiseNeighbour.IsOccupied)
                    {
                        //Debug.Log("Tile connected to upper anticlockwise neighbour");
                        //AddNeighbourHexTile(currNode.upperRowNeighbour.antiClockwiseNeighbour.occupyingHexTile);
                        currNode.upperRowNeighbour.antiClockwiseNeighbour.occupyingHexTile.ConnectHex(this);
                    }
                }
            }
            else
            {
                if (currNode.upperRowNeighbour.clockwiseNeighbour.IsOccupied)
                {
                    //Debug.Log("Tile connected to upper clockwise neighbour");
                    //AddNeighbourHexTile(currNode.upperRowNeighbour.clockwiseNeighbour.occupyingHexTile);
                    currNode.upperRowNeighbour.clockwiseNeighbour.occupyingHexTile.ConnectHex(this);
                }
                if (currNode.upperRowNeighbour.antiClockwiseNeighbour.IsOccupied)
                {
                    //Debug.Log("Tile connected to upper anticlockwise neighbour");
                    //AddNeighbourHexTile(currNode.upperRowNeighbour.antiClockwiseNeighbour.occupyingHexTile);
                    currNode.upperRowNeighbour.antiClockwiseNeighbour.occupyingHexTile.ConnectHex(this);
                }
            }
        }
    }
    void DetachFromNeighbouringHexTiles()
    {
        //Debug.Log("Hex detached from all neighbours"+this);
        foreach (HexTile hex in attachedHexTiles.ToArray())
        {
            hex.DisconnectHex(this);
        }
        attachedHexTiles.Clear();
    }

    //called externally if hex has been touched by another hex
    public void ConnectHex(HexTile otherHex)
    {
        AddNeighbourHexTile(otherHex);
        otherHex.AddNeighbourHexTile(this);
        OnHexNeighbourConnected(otherHex);
    }
    public void AddNeighbourHexTile(HexTile neighbour)
    {
        if (!attachedHexTiles.Contains(neighbour))
            attachedHexTiles.Add(neighbour);
    }

    public void DisconnectHex(HexTile otherHex)
    {
        RemoveNeighbourHexTile(otherHex);
        otherHex.RemoveNeighbourHexTile(this);
        OnHexNeighbourRemoved(otherHex);
        MarkForChecking(this);
    }
    public void RemoveNeighbourHexTile(HexTile neighbour)
    {
        if (attachedHexTiles.Contains(neighbour))
            attachedHexTiles.Remove(neighbour);
    }

    

    





    //----------------------------------------------
    //---------------Hex Movements------------------
    //----------------------------------------------

    void StartHexControlledDecent()
    {
        hexIsControllable = true;
        activeHex = this;
        StartCoroutine(HexDecentCoroutine());
    }

    IEnumerator HexDecentCoroutine()
    {
        PathHighlighter.instance.HighlightPath(currNode);
        while (hexIsControllable)
        {          
            yield return DifficultyController.hexDropDelay;
            if (hexIsControllable)
            {
                if (!CanDecend)
                {
                    HexFinishControlledDecent();
                }
                else
                {
                    MoveToLowerNode(false);
                }
            }
        }
    }

    void HexFinishControlledDecent()
    {
        //Debug.Log("Tile finished descent");
        MarkForChecking(this);

        if (shakerScript != null)
            shakerScript.ShakeGameObject(true);

        hexIsControllable = false;
        activeHex = null;
    }

    //move hex left right or down
    public void CommandHexLeft()
    {
        if (CanMoveLeft)
            MoveToLeftNode(false);
        else
            HexFinishControlledDecent();
    }
    public void CommandHexRight()
    {
        if (CanMoveRight)
            MoveToRightNode(false);
        else
            HexFinishControlledDecent();
    }
    public void CommandHexDown()
    {
        if (CanDecend)
            MoveToLowerNode(false);
        else
            HexFinishControlledDecent();
    }

    bool CanDecend
    {
        get
        {
            if (!currNode.lowerRowNeighbour.IsOccupied)
                return true;
            else
                return false;
        }
    }
    bool CanMoveRight
    {
        get
        {
            return !currNode.clockwiseNeighbour.IsOccupied;
        }
    }
    bool CanMoveLeft
    {
        get
        {
            return !currNode.antiClockwiseNeighbour.IsOccupied;
        }
    }

    //move nodes to other tiles
    void MoveToLowerNode(bool snapMovement)
    {
        AudioController.instance.PlayHexMove();

        //rotate the hex as it falls
        if (!currNode.fallsOnFlatEdge)
        {
            if (currNode.fallsClockwise)
                HexRotateLeft();
            else
                HexRotateRight();
        }

        HexNode newNode = currNode.lowerRowNeighbour;
        DetachFromNode();
        AttachToNode(newNode, snapMovement);

        //if the hex is controllable, update the path highlighter
        if (hexIsControllable)
            UpdatePathHighlighter();
    }
    void MoveToLeftNode(bool snapMovement)
    {
        AudioController.instance.PlayHexMove();

        //rotate the hex as it falls
        HexRotateRight();

        HexNode newNode = currNode.antiClockwiseNeighbour;
        DetachFromNode();
        AttachToNode(newNode, snapMovement);

        //if the hex is controllable, update the path highlighter
        if (hexIsControllable)
            UpdatePathHighlighter();
    }
    void MoveToRightNode(bool snapMovement)
    {
        AudioController.instance.PlayHexMove();

        HexRotateLeft();

        HexNode newNode = currNode.clockwiseNeighbour;
        DetachFromNode();
        AttachToNode(newNode, snapMovement);

        //if the hex is controllable, update the path highlighter
        if (hexIsControllable)
            UpdatePathHighlighter();
    }

    void UpdatePathHighlighter()
    {
        PathHighlighter.instance.HighlightPath(currNode);
    }

    //rotating right 60deg in z axis
    void HexRotateLeft()
    {
        hexModel.transform.Rotate(Vector3.forward, 60);
    }

    void HexRotateRight()
    {
        hexModel.transform.Rotate(Vector3.forward, -60);
    }

    //points the hex tile at the center once spawned
    void LookAtCenterTile()
    {
        float angle = Vector3.SignedAngle(hexModel.transform.position, tileRegister[0].transform.up, -Vector3.forward);

        //round to 60
        angle = Mathf.Round(angle / 60) * 60;

        hexModel.transform.rotation = Quaternion.Euler(hexModel.transform.rotation.eulerAngles.x, hexModel.transform.rotation.eulerAngles.y, angle);
    }




    //----------------------------------------------
    //---------------Hex Animation------------------
    //----------------------------------------------

    IEnumerator ExpandModel()
    {
        hexModel.transform.localScale = Vector3.zero;
        while (hexModel.transform.localScale.x < 0.95f)
        {
            yield return null;
            hexModel.transform.localScale += Vector3.one * Time.deltaTime * 15f;
        }
        hexModel.transform.localScale = Vector3.one;
    }
    IEnumerator ShrinkModel()
    {
        hexModel.transform.localScale = Vector3.one;
        while (hexModel.transform.localScale.x > 0.01f)
        {
            yield return null;
            hexModel.transform.localScale -= Vector3.one * Time.deltaTime * 15f;
        }
        hexModel.SetActive(false);
        hexModel.transform.localScale = Vector3.one;
        ReturnToPool();
    }


    //----------------------------------------------
    //---------------Hex Static Lists---------------
    //----------------------------------------------



    //hex queues to be actioned upon
    public static Queue<HexTile> hexTilesToBeActioned = new Queue<HexTile>();
    public static Queue<HexTile> hexTilesToBeChecked = new Queue<HexTile>();
    public static Queue<HexTile> hexTilesToBeRemoved = new Queue<HexTile>();
    public bool MarkedForChecking { get; private set; }
    public bool MarkedForRemoval { get; private set; }
    public bool MarkedForActioning { get; private set; }

    //resets all the lists
    public static void ResetAllStaticLists()
    {
        tileRegister.Clear();
        hexTilesToBeActioned.Clear();
        hexTilesToBeChecked.Clear();
        hexTilesToBeRemoved.Clear();
        activeHex = null;
        hexesAreActive = false;
    }

    HexTile GetNextCheckingHex()
    {
        HexTile hex = hexTilesToBeChecked.Dequeue();
        hex.MarkedForChecking = false;
        return hex;
    }
    HexTile GetNextActioningHex()
    {
        HexTile hex = hexTilesToBeActioned.Dequeue();
        hex.MarkedForActioning = false;
        return hex;
    }
    HexTile GetNextRemovalHex()
    {
        HexTile hex = hexTilesToBeRemoved.Dequeue();
        hex.MarkedForRemoval = false;
        return hex;
    }

    void RemoveMarks()
    {
        MarkedForChecking = false;
        MarkedForRemoval = false;
        MarkedForActioning = false;
    }

    public static void MarkForChecking(HexTile hex)
    {
        if (!hex.MarkedForChecking && !hex.MarkedForRemoval && hex.HexType!=HexType.Center)
        {
            hexTilesToBeChecked.Enqueue(hex);
            hex.MarkedForChecking = true;
        }
    }
    public static void MarkForRemoval(HexTile hex)
    {
        if (!hex.MarkedForRemoval && hex.HexType != HexType.Center)
        {
            hexTilesToBeRemoved.Enqueue(hex);
            hex.MarkedForRemoval = true;
        }
    }
    public static void MarkForActioning(HexTile hex)
    {
        if (!hex.MarkedForActioning && !hex.MarkedForRemoval && hex.HexType != HexType.Center)
        {
            hexTilesToBeActioned.Enqueue(hex);
            hex.MarkedForActioning = true;
        }
    }

}