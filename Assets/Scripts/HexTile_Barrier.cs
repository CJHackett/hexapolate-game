﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTile_Barrier : HexTile
{
    int hitsRemaining;
    const int destructionScore = 50;
    SpriteRenderer spRenderer;
    [SerializeField] GameObject onDestroyEffect;
    [SerializeField] Transform effectSpawnPoint;

    private void Awake()
    {
        spRenderer = hexModel.GetComponentInChildren<SpriteRenderer>();
        HexType = HexType.Barrier;
    }

    protected override void OnHexNeighbourConnected(HexTile otherHex)
    {
    }

    protected override void OnHexNeighbourRemoved(HexTile otherHex)
    {
        if (otherHex.IsTileDestroyed && otherHex.HexType==HexType.Plain && otherHex.CanDamageBarrier)
        {
            hitsRemaining -= currentMultiplier;

            if (hitsRemaining <= 0)
                MarkForRemoval(this);
            else
            {
                SetSpriteTransparency((float)hitsRemaining / (float)DifficultyController.currBarrierHits);
                shakerScript.ShakeGameObject(true);
            }
        }
    }

    protected override void OnHexInitiated()
    {
        hitsRemaining = DifficultyController.currBarrierHits;
        SetSpriteTransparency(1);
    }

    void SetSpriteTransparency(float value)
    {
        spRenderer.color = new Color(spRenderer.color.r, spRenderer.color.g, spRenderer.color.b, value);
    }

    protected override void OnHexDestroyed()
    {
        ObjectPool.Instance.ReuseObject(onDestroyEffect, effectSpawnPoint.position, effectSpawnPoint.rotation);
        PlayerScore.instance.UpdateScore(destructionScore * currentMultiplier);
    }

    protected override void OnHexEndOfTurnActionCallback()
    {
    }

    protected override void OnHexFinishAttaching()
    {
    }
}
