﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyController : MonoBehaviour
{
    [SerializeField] float startDropDelay = 2f;
    static float currHexDropDelay_sec;
    static float minHexDropDelay_sec = 0.1f;
    static float spawnDropDelayDecreasePerLevel_sec = 0.045f;

    [SerializeField] float startDeadDropChance = 0f;
    public static float currDeadDropChance;
    static float maxDeadDropChance = 10f;
    static float deadDropChanceIncreasePerLevel = 0.3f;

    [SerializeField][Range(1, 10)] int startBarrierHits = 1;
    public static int currBarrierHits;
    public static int levelsSinceLastBarrierIncrease;
    static int maxBarrierHits = 10;
    static int levelsPerBarrierHitIncrease = 5;

    [SerializeField] [Range(1, 10)] int startColours = 3;
    public static int currColours;
    public static int levelsSinceLastColourIncrease;
    static int levelsPerColourIncrease = 5;

    [SerializeField] [Range(1, 5)] int startNumRows = 2;
    public static int currNumRows;
    static int maxNumRows = 5;
    public static int levelsSinceLastNumRowsIncrease;
    static int levelsPerNumRowsIncrease = 15;

    public static WaitForSeconds hexDropDelay;

    public static int currentLevel;
    [Space] [SerializeField] GameObject levelTextObject;
    static Text levelText;

    [Header("Debug")]
    [SerializeField] [Range(0, 100)] int skipToLevel = 0;


    const int mediumDifficultyUnlockPoint = 19;
    const int hardDifficultyUnlockPoint = 39;
    public static bool difficultyLoaded;

    private void Awake()
    {
        difficultyLoaded = false;
    }

    void Start()
    {
        levelText = levelTextObject.GetComponent<Text>();
        if (skipToLevel > 0)
            SkipToLevel(skipToLevel);
        else
            SkipToLevel(GameSettings.instance.StartLevel);
        UpdateLevelText();
        difficultyLoaded = true;
    }

    public static void IncreaseDifficulty()
    {
        currentLevel++;

        if (currentLevel == mediumDifficultyUnlockPoint && !GameSettings.instance.MediumUnlocked)
            GameSettings.instance.MediumUnlocked = true;
        if (currentLevel == hardDifficultyUnlockPoint && !GameSettings.instance.HardUnlocked)
            GameSettings.instance.HardUnlocked = true;

        levelsSinceLastBarrierIncrease++;
        if (levelsSinceLastBarrierIncrease >= levelsPerBarrierHitIncrease)
        {
            levelsSinceLastBarrierIncrease = 0;
            currBarrierHits++;
            if (currBarrierHits > maxBarrierHits)
                currBarrierHits = maxBarrierHits;
        }

        levelsSinceLastColourIncrease++;
        if (levelsSinceLastColourIncrease >= levelsPerColourIncrease)
        {
            levelsSinceLastColourIncrease = 0;
            currColours++;
            if (currColours > HexGenerator.instance.AvailableColours)
                currColours = HexGenerator.instance.AvailableColours;
        }

        currDeadDropChance += deadDropChanceIncreasePerLevel;
        if (currDeadDropChance > maxDeadDropChance)
            currDeadDropChance = maxDeadDropChance;

        currHexDropDelay_sec -= spawnDropDelayDecreasePerLevel_sec;
        if (currHexDropDelay_sec < minHexDropDelay_sec)
            currHexDropDelay_sec = minHexDropDelay_sec;
        hexDropDelay = new WaitForSeconds(currHexDropDelay_sec);

        levelsSinceLastNumRowsIncrease++;
        if (levelsSinceLastNumRowsIncrease >= levelsPerNumRowsIncrease)
        {
            levelsSinceLastNumRowsIncrease = 0;
            currNumRows++;
            if (currNumRows > maxNumRows)
                currNumRows = maxNumRows;
        }

        UpdateLevelText();
    }

    static void UpdateLevelText()
    {
        levelText.text = (currentLevel+1).ToString();
    }

    void ResetAllDifficulties()
    {
        currentLevel = 0;
        currColours = startColours;
        levelsSinceLastBarrierIncrease = 0;
        currDeadDropChance = startDeadDropChance;
        currHexDropDelay_sec = startDropDelay;
        currBarrierHits = startBarrierHits;
        currNumRows = startNumRows;
        hexDropDelay = new WaitForSeconds(currHexDropDelay_sec);

        levelsSinceLastBarrierIncrease = 0;
        levelsSinceLastColourIncrease = 0;
        levelsSinceLastNumRowsIncrease = 0;
    }

    void SkipToLevel(int level)
    {
        ResetAllDifficulties();
        for (int i = 0; i < level; i++)
            IncreaseDifficulty();
    }
}
