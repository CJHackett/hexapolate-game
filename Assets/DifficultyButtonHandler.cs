﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyButtonHandler : MonoBehaviour
{
    [SerializeField] Button easyButton;
    [SerializeField] Button mediumButton;
    [SerializeField] Button hardButton;
    [SerializeField] Image easySelectedIndicator;
    [SerializeField] Image mediumSelectedIndicator;
    [SerializeField] Image hardSelectedIndicator;
    [SerializeField] MainMenuNavigation menuScript;
    [SerializeField] Color lockedColor;
    [SerializeField] Color normalColor;
    [SerializeField] Color pressedColor;

    // Start is called before the first frame update
    void Start()
    {
        if (!GameSettings.instance.MediumUnlocked)
        {
            ColorBlock cb = mediumButton.colors;
            cb.normalColor = lockedColor;
            cb.highlightedColor = lockedColor;
            cb.selectedColor = lockedColor;
            cb.pressedColor = lockedColor;
            cb.disabledColor = lockedColor;
            mediumButton.colors = cb;
        }
        else
        {
            ColorBlock cb = mediumButton.colors;
            cb.normalColor = normalColor;
            cb.highlightedColor = normalColor;
            cb.selectedColor = normalColor;
            cb.pressedColor = pressedColor;
            cb.disabledColor = lockedColor;
            mediumButton.colors = cb;
        }

        if (!GameSettings.instance.HardUnlocked)
        {
            ColorBlock cb = hardButton.colors;
            cb.normalColor = lockedColor;
            cb.highlightedColor = lockedColor;
            cb.selectedColor = lockedColor;
            cb.pressedColor = lockedColor;
            cb.disabledColor = lockedColor;
            hardButton.colors = cb;
        }
        else
        {
            ColorBlock cb = hardButton.colors;
            cb.normalColor = normalColor;
            cb.highlightedColor = normalColor;
            cb.selectedColor = normalColor;
            cb.pressedColor = pressedColor;
            cb.disabledColor = lockedColor;
            hardButton.colors = cb;
        }

        switch (GameSettings.instance.Difficulty)
        {
            case GameDifficulty.Easy:
                EasySelected();
                break;
            case GameDifficulty.Medium:
                MediumSelected();
                break;
            case GameDifficulty.Hard:
                HardSelected();
                break;
        }
    }

    public void EasyButtonPressed()
    {
        GameSettings.instance.Difficulty = GameDifficulty.Easy;
        EasySelected();
    }
    public void MediumButtonPressed()
    {
        if (GameSettings.instance.MediumUnlocked)
        {
            GameSettings.instance.Difficulty = GameDifficulty.Medium;
            MediumSelected();
        }
        else
        {
            menuScript.OpenNotificationPanel("THIS DIFFICULTY WILL BE UNLOCKED WHEN YOU HAVE REACHED LEVEL 20");
        }
    }
    public void HardButtonPressed()
    {
        if (GameSettings.instance.HardUnlocked)
        {
            GameSettings.instance.Difficulty = GameDifficulty.Hard;
            HardSelected();
        }
        else
        {
            menuScript.OpenNotificationPanel("THIS DIFFICULTY WILL BE UNLOCKED WHEN YOU HAVE REACHED LEVEL 40. GOOD LUCK!");
        }
    }

    void EasySelected()
    {
        easySelectedIndicator.enabled = true;
        mediumSelectedIndicator.enabled = false;
        hardSelectedIndicator.enabled = false;
    }
    void MediumSelected()
    {
        easySelectedIndicator.enabled = false;
        mediumSelectedIndicator.enabled = true;
        hardSelectedIndicator.enabled = false;
    }
    void HardSelected()
    {
        easySelectedIndicator.enabled = false;
        mediumSelectedIndicator.enabled = false;
        hardSelectedIndicator.enabled = true;
    }

}
